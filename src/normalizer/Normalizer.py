#!/usr/bin/python

import os
import Host
from pygrok import Grok

class BaseNormalizer(object):

        # Grok Patterns
        __pattern_hostname = "%{WORD:hostname}"
        __pattern_uname = "%{WORD:OperatingSystem} %{GREEDYDATA:OtherJunk}"
        __pattern_lparstat = "%{GREEDYDATA:Name}:%{GREEDYDATA:Value}"
        __pattern_oslevel = "%{GREEDYDATA:Version}"

        # File names
        __filename_hostname = "001.hostname.out"
        __filename_uname = "001.uname_a.out"
        __filename_lparstat = "001.lparstat_i.out"
        __filename_oslevel = "001.oslevel.out"
        
        def __init__(self, hostdirectory):
                assert (os.path.isdir(hostdirectory) == True), "Host Folder " + hostdirectory + "does not exist"
                self.hostDirectory = hostdirectory
                self.discoveredHost = Host.A1DiscoveredHost()

                self.grokHostname = Grok(self.__pattern_hostname)
                self.grokUname = Grok(self.__pattern_uname)
                self.groklparstat = Grok(self.__pattern_lparstat)
                self.grokOslevel = Grok(self.__pattern_oslevel)

                return

        def discover(self):
                
                self.discoverHostname()
                self.discoverOSVersion()
                self.discoverOS()
                self.discoverLparstat()

		self.discoverNetworkInterfaces()

                self.discoveredHost.print()
                return        

	def discoverNetworkInterfaces(self):
		# No Base class implementation
		return

        def discoverHostname(self):
                reqdfile = self.hostDirectory + "/" + self.__filename_hostname
                hostname = self.discoverAttribute(reqdfile, self.grokHostname, 'hostname')
                self.discoveredHost.setHostName(hostname)
                return                
        
        def discoverOSVersion(self):
                reqdfile = self.hostDirectory + "/" + self.__filename_oslevel
                version = self.discoverAttribute(reqdfile, self.grokOslevel, 'Version')
                self.discoveredHost.setOSVersion(version)
                return
        
        def discoverOS(self):
                reqdfile = self.hostDirectory + "/" + self.__filename_uname
                os = self.discoverAttribute(reqdfile, self.grokUname, 'OperatingSystem')
                self.discoveredHost.setOperatingSystem(os)
                return
        
        def discoverLparstat(self):
                reqdfile = self.hostDirectory + "/" + self.__filename_lparstat
                assert (os.path.isfile(reqdfile) == True), "File: " % reqdfile % " doesn't exist. Cannot discover LPARSTAT data"
                
                with open (reqdfile) as filecontent:
                        for line in filecontent:
                                mtch = self.groklparstat.match(line.strip())
                                self.discoveredHost.addLparStatData(str(mtch['Name']).strip(), str(mtch['Value']).strip())
                return 
                

        def discoverAttribute(self, reqdfile, grk, parameter):

                assert (os.path.isfile(reqdfile) == True), "File: " % reqdfile % " doesn't exist. Cannot discover " % parameter
                
                data = ""
                with open(reqdfile, "r") as filecontent:
                        data = filecontent.read()
                
                mtch = grk.match(data)
                return mtch[parameter]
        
