#!/usr/bin/python

# imports
import os
import DiscoveryFactory

class A1Discovery:
	
	def __init__(self, rootdirectory):
		assert (os.path.isdir(rootdirectory) == True), "Root directory does not exist"

		self.rootdirectory = rootdirectory
		self.discoveryFactory = DiscoveryFactory.A1DiscoveryFactory()
		for subfolder in os.listdir(self.rootdirectory):
			normalizer = self.discoveryFactory.getNormalizer(self.rootdirectory + "/" + subfolder)
			normalizer.discover()

a1disco = A1Discovery('/home/workspace/autodisco/normalizer/discovery/')
