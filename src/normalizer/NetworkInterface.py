#!/usr/bin/python

from DiscoveredEntity import A1DiscoveredEntity

class A1NetworkInterface(A1DiscoveredEntity):
	
	def setNetworkIntefaceName(self, name):
		self.networkIfName = name

	def setIPAddress(self, ipaddr)
		self.ipaddress = ipaddr
	
	def setNetmask(self, netmask)
		self.netmask = netmask

	def setBroadcast(self, broadcast)
		self.broadcase = broadcast

	def setTcpSendSpace(self, tcpsendspace)
		self.tcpSendSpace = tcpsendspace

	def setTcpRecvSpace(self, tcprecvspace)
		self.tcpRecvSpace = tcprecvspace

	def setRfc1323(self, rfc)
		self.rfc1323 = rfc
