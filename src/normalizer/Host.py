#!/usr/bin/python

import datetime
from DiscoveredEntity import A1DiscoveredEntity

class A1DiscoveredHost(A1DiscoveredEntity):
	
	def __init__(self):
		super(A1DiscoveredHost, self).__init__()		
		self.lparstat = {}
		self.mntdevs = {}
		self.networkinterfaces = {}
		return	

	def setHostName(self, hostname):
		self.hostname = hostname
		return

	def setOperatingSystem(self, os):
		self.operatingSystem = os
		return

	def setOSVersion(self, version):
		self.osversion = version
		return

	def addLparStatData(self, name, value):
		self.lparstat[name] = value
		return

	def addNetworkInterface(self, name, interface):
		self.networkinterfaces[name] = interface
		return

	def print(self):
		print (self.discoveredTimestamp)
		print (self.hostname)
		print (self.operatingSystem)
		print (self.osversion)
		print (self.lparstat)	
