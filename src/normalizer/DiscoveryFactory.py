#!/usr/bin/python

import os
from Normalizer import BaseNormalizer
import io
import importlib
from pygrok import Grok

class A1DiscoveryFactory:
	__normalizermapping = "normalizer.csv"
	__pattern = "%{WORD:OperatingSystem},%{GREEDYDATA:Version},%{WORD:Package},%{WORD:NormalizerClass}"

	def __init__(self):
		self.unamefilename = "001.uname_a.out"
		self.oslevelfilename = "001.oslevel.out"
		self.readConfiguration()
		return

	def readConfiguration(self):
		self.mappings = []
		grok = Grok(self.__pattern)
		lines = tuple(open(self.__normalizermapping, "r"))
		lines = [line.rstrip('\n') for line in lines]
		for row in lines:
			grokkedRow = grok.match(row)
			self.mappings.append(grokkedRow)

	def getNormalizer(self, foldername):
		assert(os.path.isdir(foldername) == True), "Folder [" + foldername + "] does not exist"
		
		fUname = open(foldername + '/' + self.unamefilename,'r')
		unamedata = fUname.readline()
		unamedatasplit = unamedata.split()
		osname = unamedatasplit[0]
		fUname.close()

		fOslevel = open(foldername + '/' + self.oslevelfilename, 'r')
		oslevel = fOslevel.readline()
		oslevel = oslevel.rstrip('\n')
		fOslevel.close()
	
		try:
			packageName = ""
			className = ""
			for row in self.mappings:
				if osname == row['OperatingSystem'] and oslevel == row['Version']:
					packageName = row['Package']
					className = row['NormalizerClass']
					break
			#print ("Identified Package Name: " + packageName + " and Class Name: " + className)
			clazz = getattr(importlib.import_module(packageName), className)
			normalizer = clazz(foldername)			
			return normalizer
		except Exception as e:
			print ("Invalid / No dedicated Normalizer found for Platform [" +  osname + "] and version [" + oslevel + "]")
			print (e)
