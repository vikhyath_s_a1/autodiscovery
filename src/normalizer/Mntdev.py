#!/usr/bin/python

import DiscoveredEntity

class A1DiscoveredHostMntdev(A1DiscoveredEntity):

	def setMountedOver(self, mountedOver):
		self.mountedOver = mountedOver
	
	def setMounted(self, mounted):
		self.mounted = mounted

	def setVfs(self, vfs):
		self.vfs = vfs

	def setDate(self, date):
		self.date = date

	def setOptions(self, options):
		self.options = options

	def setMbBlocks(self, mbblocks):
		self.mbBlocks = mbblocks

	def setFree(self, free):
		self.free = free

	def setPercentageUsed(self, perctuse):
		self.percentageUsed = perctuse

	def setIUsed(self, iused):
		self.iUsed = iused

	def setPercentageIUsed(self, periuse):
		self.percentageIUsed = periuse		
