package com.appnomic.appsone;

import java.util.List;

import lombok.Data;

@Data
public class Host{
	private String id;
	private String name;
	private String serialNo;
	private String os;
	private String platform;
	private List<String> ip;
}