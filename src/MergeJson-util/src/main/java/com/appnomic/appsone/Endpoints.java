package com.appnomic.appsone;

import lombok.Data;

@Data
public class Endpoints{
	private String id;
	private String ip;
	private String port;
	private String hostId;
}