package com.appnomic.appsone;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.opencsv.CSVWriter;

public class MergeProbe {

	public static void main(String[] args) {

		String path = Paths.get(".").toAbsolutePath().normalize().toString();

		File folder = new File(path+"/TestFiles");
		//		File folder = new File(args[0]);

		File[] listOfFiles = folder.listFiles();

		List<Host> hosts = new ArrayList<Host>();
		List<Processes> processes = new ArrayList<Processes>();
		List<Endpoints> endpointsList = new ArrayList<Endpoints>();
		List<Connections> connections = new ArrayList<Connections>();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				System.out.println("File " + listOfFiles[i].getName());

				try {
					JSONParser parser = new JSONParser();

					JSONObject object = new JSONObject();

					object = (JSONObject) parser.parse(new FileReader(listOfFiles[i]));

					if (object.containsKey("data")) {
						JSONObject dataObject = (JSONObject) object.get("data");
						if (dataObject.containsKey("nodeId")) {

							String hostNodeId = dataObject.get("nodeId").toString();
							boolean isPresent = false;
							Host hosts2 = null;
							for(Host host : hosts) {
								if (host.getId().equalsIgnoreCase(hostNodeId)) {
									hosts2 = host;
									isPresent = true;
								}
							}

							List<String> ips = new ArrayList<String>();
							if (dataObject.containsKey("networkInterfaces")) {
								JSONArray ipArr = (JSONArray) dataObject.get("networkInterfaces");
								for(int j = 0; j<ipArr.size(); j++) {
									JSONObject ipObj = (JSONObject) ipArr.get(j);
									if (ipObj != null && ipObj.containsKey("interfaceIP")) {
										ips.add(ipObj.get("interfaceIP").toString());
									}
								}
							}

							if (hosts2 == null) {
								hosts2 = new Host();
								hosts2.setIp(ips);
								hosts2.setId(hostNodeId);
							}else {
								List<String> ips2 = hosts2.getIp();
								hosts.remove(hosts2);
								hosts2 = new Host();
								for(String ip : ips) {
									if (!ips2.contains(ip)) {
										ips2.add(ip);
									}
								}
								hosts2.setIp(ips2);
								hosts2.setId(hostNodeId);
							}
							hosts2.setName(dataObject.get("hostname").toString());
							hosts2.setOs(dataObject.get("operatingSystem").toString());
							hosts2.setPlatform(dataObject.get("platform").toString());
							hosts2.setSerialNo(dataObject.get("serialNumber").toString());								

							hosts.add(hosts2);

							if (dataObject.containsKey("runningProcesses")) {
								JSONArray processArr = (JSONArray) dataObject.get("runningProcesses");
								if (isPresent) {
									List<Processes> hostProcesses = new ArrayList<Processes>();
									for(Processes processes2 : processes) {
										if(processes2.getHostId().equalsIgnoreCase(hostNodeId))
											hostProcesses.add(processes2);
									}

									for(int j = 0; j<processArr.size(); j++) {
										JSONObject processObj = (JSONObject) processArr.get(j);

										String processId = processObj.get("processId").toString();
										if (!processes.stream().filter(o -> o.getId().equals(processId)).findFirst().isPresent()) {
											Processes process = new Processes();
											process.setHostId(hostNodeId);
											process.setId(processObj.get("processId").toString());
											process.setComponentName(processObj.get("processName").toString());
											process.setArgu(processObj.get("processArgs").toString());
											process.setName(processObj.get("processName").toString());
											processes.add(process);
										}
									}

								}else {
									for(int j = 0; j<processArr.size(); j++) {
										JSONObject processObj = (JSONObject) processArr.get(j);
										Processes process = new Processes();
										process.setHostId(hostNodeId);
										process.setId(processObj.get("processId").toString());
										process.setComponentName(processObj.get("processName").toString());
										process.setArgu(processObj.get("processArgs").toString());
										process.setName(processObj.get("processName").toString());
										processes.add(process);
									}
								}
							}

							if (dataObject.containsKey("endpoints")) {
								JSONArray endpointArr = (JSONArray) dataObject.get("endpoints");
								if (isPresent) {
									List<Endpoints> endPoints = new ArrayList<Endpoints>();
									for(Endpoints endpoint : endpointsList) {
										if(endpoint.getHostId().equalsIgnoreCase(hostNodeId))
											endPoints.add(endpoint);
									}

									for(int j = 0; j<endpointArr.size(); j++) {
										JSONObject endPointObj = (JSONObject) endpointArr.get(j);

										String processId = endPointObj.get("nodeId").toString();
										if (!endPoints.stream().filter(o -> o.getId().equals(processId)).findFirst().isPresent()) {
											Endpoints endpoints = new Endpoints();
											endpoints.setHostId(hostNodeId);
											endpoints.setId(processId);
											endpoints.setPort((endPointObj.get("portNo") != null)? endPointObj.get("portNo").toString() : "");
											endpoints.setIp((endPointObj.get("ipAddress") != null)? endPointObj.get("ipAddress").toString() : "");
											endpointsList.add(endpoints);
										}
									}

								}else {
									for(int j = 0; j<endpointArr.size(); j++) {
										JSONObject endPointObj = (JSONObject) endpointArr.get(j);
										Endpoints endpoints = new Endpoints();
										endpoints.setHostId(hostNodeId);
										endpoints.setId(endPointObj.get("nodeId").toString());
										endpoints.setPort((endPointObj.get("portNo") != null)? endPointObj.get("portNo").toString() : "");
										endpoints.setIp((endPointObj.get("ipAddress") != null)? endPointObj.get("ipAddress").toString() : "");
										endpointsList.add(endpoints);

									}
								}
							}

							if (dataObject.containsKey("connections")) {
								JSONArray connectionArr = (JSONArray) dataObject.get("connections");
								if (isPresent) {
									List<Connections> outgoingConnections = new ArrayList<Connections>();
									for(Connections connections2 : connections) {
										if (connections2.getHostId().equalsIgnoreCase(hostNodeId))
											outgoingConnections.add(connections2);
									}

									for(int j = 0; j<connectionArr.size(); j++) {
										JSONObject connectionObj = (JSONObject) connectionArr.get(j);

										String processId = connectionObj.get("nodeId").toString();
										if (!outgoingConnections.stream().filter(o -> o.getNodeId().equals(processId)).findFirst().isPresent()) {
											Connections connections1 = new Connections();
											connections1.setHostId(hostNodeId);
											connections1.setNodeId(connectionObj.get("nodeId").toString());
											connections1.setConnectionType((connectionObj.get("connectionType") != null)? connectionObj.get("connectionType").toString() : "");
											connections1.setDirection((connectionObj.get("direction") != null)? connectionObj.get("direction").toString() : "");
											connections1.setLocalIP((connectionObj.get("localIP") != null)? connectionObj.get("localIP").toString() : "");
											connections1.setLocalPort((connectionObj.get("localPort") != null)? connectionObj.get("localPort").toString() : "");
											connections1.setRemoteIP((connectionObj.get("remoteIP") != null)? connectionObj.get("remoteIP").toString() : "");
											connections1.setRemotePort((connectionObj.get("remotePort") != null)? connectionObj.get("remotePort").toString() : "");
											connections.add(connections1);
										}
									}

								}else {
									for(int j = 0; j<connectionArr.size(); j++) {
										JSONObject connectionObj = (JSONObject) connectionArr.get(j);

										Connections connections1 = new Connections();
										connections1.setHostId(hostNodeId);
										connections1.setNodeId(connectionObj.get("nodeId").toString());
										connections1.setConnectionType((connectionObj.get("connectionType") != null)? connectionObj.get("connectionType").toString() : "");
										connections1.setDirection((connectionObj.get("direction") != null)? connectionObj.get("direction").toString() : "");
										connections1.setLocalIP((connectionObj.get("localIP") != null)? connectionObj.get("localIP").toString() : "");
										connections1.setLocalPort((connectionObj.get("localPort") != null)? connectionObj.get("localPort").toString() : "");
										connections1.setRemoteIP((connectionObj.get("remoteIP") != null)? connectionObj.get("remoteIP").toString() : "");
										connections1.setRemotePort((connectionObj.get("remotePort") != null)? connectionObj.get("remotePort").toString() : "");
										connections.add(connections1);

									}
								}
							}
						}
					}

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}

				try {
					FileWriter fileWriter = new FileWriter("./Hosts.csv");
					CSVWriter csvWriter = new CSVWriter(fileWriter);
					csvWriter.writeAll(HostArray(hosts));
					csvWriter.flush();
					fileWriter.flush();
				} catch (Exception e) {
					System.out.println("Error creating csv file! " + e.getMessage());
				}

				try {
					FileWriter fileWriter = new FileWriter("./Processes.csv");
					CSVWriter csvWriter = new CSVWriter(fileWriter);
					csvWriter.writeAll(processArray(processes));
					csvWriter.flush();
					fileWriter.flush();
				} catch (Exception e) {
					System.out.println("Error creating csv file! " + e.getMessage());
				}

				try {
					FileWriter fileWriter = new FileWriter("./Endpoints.csv");
					CSVWriter csvWriter = new CSVWriter(fileWriter);
					csvWriter.writeAll(endPointArray(endpointsList));
					csvWriter.flush();
					fileWriter.flush();
				} catch (Exception e) {
					System.out.println("Error creating csv file! " + e.getMessage());
				}

				try {
					FileWriter fileWriter = new FileWriter("./Outgoing connections.csv");
					CSVWriter csvWriter = new CSVWriter(fileWriter);
					csvWriter.writeAll(connectionArray(connections));
					csvWriter.flush();
					fileWriter.flush();
				} catch (Exception e) {
					System.out.println("Error creating csv file! " + e.getMessage());
				}

			} 
		}

	}

	public static List<String[]> HostArray(List<Host> Hosts) {
		List<String[]> records = new ArrayList<String[]>();

		// adding header record
		records.add(new String[] { "Host Node ID", "Hostname", "Serial Number", "Operating System", "Platform", "IP address (all)" });

		Iterator<Host> it = Hosts.iterator();
		while (it.hasNext()) {
			Host host = it.next();
			records.add(new String[] { host.getId(), host.getName(), host.getSerialNo(), host.getOs(), host.getPlatform(), String.join(",", host.getIp())});
		}
		return records;
	}

	public static List<String[]> processArray(List<Processes> processes) {
		List<String[]> records = new ArrayList<String[]>();

		// adding header record
		records.add(new String[] { "Process Node ID", "Process Name", "Process Arguments", "Component Name", "Host Node ID (FK)" });

		Iterator<Processes> it = processes.iterator();
		while (it.hasNext()) {
			Processes host = it.next();
			records.add(new String[] { host.getId(), host.getName(), host.getArgu(), host.getComponentName(), host.getHostId()});
		}
		return records;
	}

	public static List<String[]> connectionArray(List<Connections> connections) {
		List<String[]> records = new ArrayList<String[]>();

		// adding header record
		records.add(new String[] { "Host Node ID", "Process node Id", "Connection Type", "Direction", "Local IP", "Local Port", "Remote IP", "Remote Port"});

		Iterator<Connections> it = connections.iterator();
		while (it.hasNext()) {
			Connections host = it.next();
			records.add(new String[] { host.getHostId(), host.getNodeId(), host.getConnectionType(), host.getDirection(), host.getLocalIP(), host.getLocalPort(), host.getRemoteIP(), host.getRemotePort()});
		}
		return records;
	}

	public static List<String[]> endPointArray(List<Endpoints> endpoints) {
		List<String[]> records = new ArrayList<String[]>();

		// adding header record
		records.add(new String[] { "Endpoint Node ID", "IP", "Port", "Host Node ID (FK)" });

		Iterator<Endpoints> it = endpoints.iterator();
		while (it.hasNext()) {
			Endpoints host = it.next();
			records.add(new String[] { host.getId(), host.getIp(), host.getPort(), host.getHostId()});
		}
		return records;
	}

}






