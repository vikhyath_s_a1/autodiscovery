package com.appnomic.appsone;

import lombok.Data;

@Data
public class Processes{
	private String id;
	private String name;
	private String argu;
	private String componentName;
	private String hostId;
}