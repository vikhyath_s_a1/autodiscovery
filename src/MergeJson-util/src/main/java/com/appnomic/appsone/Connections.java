package com.appnomic.appsone;

import lombok.Data;

@Data
public class Connections{
	private String hostId;
	private String nodeId;
	private String localPort;
	private String localIP;
	private String remoteIP;
	private String remotePort;
	private String connectionType;
	private String direction;
}