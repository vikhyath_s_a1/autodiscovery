#!/usr/bin/env bash

#1)	Takes no arguments
#2)	Open the script and update variable setting of RUN_NUMLOOPS and RUN_INTERVAL, at the start of the script
#3)	Run from within the work/ sub-directory – cd work; ../rundcap.sh;
#4)	It creates a sub-directory called rundcap_<YYYYMMDDHHMISS>_${RUN_NUMLOOPS}loops_${RUN_INTERVAL}_secs/
#5)	Finally, after the looping and sleeping, it tars and gzips this entire folder
#6)	Echoes whatever it does, with a final message asking user to ship the above rundcap_...._secs.tgz for processing
#7)	Good idea to run this script using nohup (nohup ../rundcap.sh 2>&1 &)

# Modify the below 2 params to set different defaults
# RUN_NUMLOOPS - number of times to execute ${RUN_COMMAND}
# RUN_INTERVAL - number of seconds to sleep in between executions

RUN_NUMLOOPS=5
RUN_INTERVAL=300
export RUN_NUMLOOPS
export RUN_INTERVAL

if [ $# -gt 0 ]
then
  RUN_NUMLOOPS=$1
  shift
fi

if [ $# -gt 0 ]
then
  RUN_INTERVAL=$1
  shift
fi

echo "Got input NUMLOOPS [${RUN_NUMLOOPS} loops] and INTERVAL [${RUN_INTERVAL} seconds]..."

# RUN_COMMAND - command to execute ${RUN_NUMLOOPS} times,
#               while sleeping ${RUN_INTERVAL} seconds in between executions
RUN_COMMAND=a1dcap.sh
export RUN_COMMAND

fnEnsureAbsolute()
{
  PathName=$1
  PathValue=$2

  if [[ ${PathValue} == /* ]]
  then
    echo "# Absolute path supplied: ${PathName}=[${PathValue}] Using it as is."
    PathValue="${PathValue}"
    export $PathName="$PathValue"
  else
    RelPathValue=${PathValue}
    PathValue="`pwd`/${PathValue}"
    export $PathName="$PathValue"
    echo "# Relative path supplied: ${PathName}=[${RelPathValue}]"
    echo "#   Changed to absolute path [${PathValue}]"
  fi
}


PROGNAME=$0
BASEDIR=`dirname ${PROGNAME}`
export BASEDIR
fnEnsureAbsolute BASEDIR ${BASEDIR}

YYYYMMDDHHMISS=`date +"%Y%m%d%H%M%S"`
export YYYYMMDDHHMISS

DUMPDIRFORTHERUN="rundcap_${YYYYMMDDHHMISS}_${RUN_NUMLOOPS}loops_${RUN_INTERVAL}secs"
mkdir ${DUMPDIRFORTHERUN}
if [ -d ${DUMPDIRFORTHERUN} ]
then
  echo "Created dump directory [${DUMPDIRFORTHERUN}] for the run..."
else
  echo "Error creating dump directory [${DUMPDIRFORTHERUN}]..."
  exit 1
fi

cd ${DUMPDIRFORTHERUN}

LOOPS_LEFT=${RUN_NUMLOOPS}
COUNTER=0

while [ ${LOOPS_LEFT} -gt 0 ]
do
  COUNTER=`expr ${COUNTER} + 1`
  echo "Loop # ${COUNTER} START"
  echo "==============================================================="
  echo
  ${BASEDIR}/${RUN_COMMAND}
  echo
  echo "==============================================================="
  echo "Loop # ${COUNTER} END"

  SKIPOPENFILEDETAILS=SkipOpenFileDetails
  export SKIPOPENFILEDETAILS

  LOOPS_LEFT=`expr ${LOOPS_LEFT} - 1`
  if [ ${LOOPS_LEFT} -eq 0 ]
  then
    echo "Completed the run. Exitting..."
    break
  fi

  echo "${LOOPS_LEFT} loops left..."
  echo "Sleeping for ${RUN_INTERVAL} seconds..."
  sleep ${RUN_INTERVAL}
done

cd ..

echo "Archiving the output from the run..."

# Block added by Sandesh Rane
# - For appending IP address of the host to dump directory name
OS_NAME=`uname` 
if [ $OS_NAME = "Linux" ] 
then 
  CLIENT_IP=$(grep `hostname` /etc/hosts | grep -Ev "^#"| head -n 1 | awk '{ print $1}') 
elif [ $OS_NAME = "AIX" ] 
then 
  CLIENT_IP=$(grep `hostname` /etc/hosts | grep -Ev "^#"| head -n 1 | awk '{ print $1}') 
elif [ $OS_NAME = "SunOS" ] 
then 
  CLIENT_IP=$(ping -s  `hostname` 1 2|grep bytes|tail -1|awk {'print $5'}|tr -d "("|tr -d ")"|tr -d ":") 
else 
  CLIENT_IP=`hostname` 
fi 

NEW_DUMPDIRFORTHERUN="${DUMPDIRFORTHERUN}_${CLIENT_IP}"
mv ${DUMPDIRFORTHERUN} ${NEW_DUMPDIRFORTHERUN}
tar -zcvf ${NEW_DUMPDIRFORTHERUN}.tar.gz ${NEW_DUMPDIRFORTHERUN}

echo
echo "Please send ${NEW_DUMPDIRFORTHERUN}.tar.gz for processing..."
echo "Thank you. Have a nice day!"
echo


