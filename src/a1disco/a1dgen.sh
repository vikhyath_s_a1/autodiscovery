#!/usr/bin/env bash

PROGNAME=$0
PROGBASENAME=`basename ${PROGNAME}`
BASEDIR=`dirname ${PROGNAME}`
LIBDIR=${BASEDIR}/lib
CFGDIR=${BASEDIR}/config
WRKDIR=${BASEDIR}/work
CMPDIR=${BASEDIR}/components

export BASEDIR
export LIBDIR
export CFGDIR
export WRKDIR

YYYYMMDDHHMISS=`date +"%Y%m%d%H%M%S"`
export YYYYMMDDHHMISS

SELFLOGFILE="${PROGBASENAME//.sh/}_${YYYYMMDDHHMISS}-outerr.log"
export SELFLOGFILE

fnmain()
{

RAWCAPDIR=`pwd`
export RAWCAPDIR

if [ $# -eq 0 ]
then
  RAWCAPDIR=`pwd`
  linfo "Using current working directory to read raw captured data ..."
else
  RAWCAPDIR=$1
  cd "${RAWCAPDIR}"
  linfo "Using supplied directory to read raw captured data ..."
fi
linfo "  RAWCAPDIR : [${RAWCAPDIR}]"

UNAME=`cut -f1 -d' ' 001.uname_a.out`
if [ "${UNAME}" = "Linux" ]
then
  UNAME=`cat 001.lsb_release_drci.out | grep "Distributor ID" | awk -F ':' '
    {
      gsub(/^[ \t]*/, "", $2);
      gsub(/[ \t]*$/, "", $2);
      print $2;
    }'`
fi
export UNAME
linfo "OS Platform (uname): ${UNAME}"

linfo "Loading physical component extraction functions..."
for file in ${CMPDIR}/common/*.sh ${CMPDIR}/${UNAME}/*.sh
do
  linfo "  sourcing [${file}]..."
  . ${file}
done

HOST_ID=`basename ${RAWCAPDIR}`
#                            YYYYMMDDHHMISS
HOST_ID=${HOST_ID//${A1DCAP}_??????????????_/}
export HOST_ID
linfo "Host ID (lowest observed NIC MAC address): ${HOST_ID}"

OBSRVTS=`basename ${RAWCAPDIR}`
OBSRVTS=${OBSRVTS//${A1DCAP}_/}
OBSRVTS=${OBSRVTS%%_*}
export OBSRVTS
linfo "Observation Timestamp (YYYYMMDDHHMISS): ${OBSRVTS}"


linfo "#-------------------------------------------------------------------------------"
linfo "Using following files to create (:HOST) ..."
for file in 001.*
do
  linfo "  [$file]"
done
linfo "Extracting (:HOST) entity details..."
fn_LABEL_SUFFIX HOST

linfo "#-------------------------------------------------------------------------------"
linfo "Using following files to create (:NIC)s, (:IPADDRESS)es ..."
for file in 002.*
do
  linfo "  [$file]"
done
linfo "Extracting (:NIC)s', (:IPADDRESS)es' entity details..."
fn_LABEL_SUFFIX NIC

linfo "#-------------------------------------------------------------------------------"
linfo "Using following files to create (:MNTDEV)s ..."
for file in 003.*
do
  linfo "  [$file]"
done
linfo "Extracting (:MNTDEV)s' entity details..."
fn_LABEL_SUFFIX MNTDEV

linfo "#-------------------------------------------------------------------------------"
linfo "Using following files to create (:PROCESS)es, (:IPADDRESS)es, (:SOCKADDRESS)es ..."
for file in 004.*
do
  linfo "  [${file}]"
done
linfo "Extracting (:PROCESS)es', (:IPADDRESS)es' and (:SOCKADDRESS)es' entity details..."
fn_LABEL_SUFFIX PROCESS_ADDRESS

linfo "#-------------------------------------------------------------------------------"
linfo "Using following files to create (:PATH)s ..."
for file in 005.* 006.*
do
  linfo "  [${file}]"
done
linfo "Extracting (:PROCESS)es', (:PATH)s' entity details..."
fn_LABEL_SUFFIX PROCESS_PATH

linfo "#-------------------------------------------------------------------------------"
linfo "Putting together all generated data as an XLSX workbook"
python ${BASEDIR}/csvtoxlsx.py ntts-${HOST_ID}-${OBSRVTS}.xlsx *.csv
linfo "#-------------------------------------------------------------------------------"
}

fnredirmain()
{

. ${LIBDIR}/common/lib.sh
. ${LIBDIR}/common/logging.sh
fnuname

linfo "Loading common and platform specific shell function library modules ..."
for file in ${LIBDIR}/common/*.sh ${LIBDIR}/${UNAME}/*.sh
do
  linfo "  sourcing [${file}]..."
  . ${file}
done

trap 'fnsiginchild' SIGTERM SIGINT

PREREQS="which printf uname hostname awk cut cat grep egrep ls find mv rm sort uniq head tail netstat ifconfig lsof"
export PREREQS
fnaddprs

RUNASDAEMON=0
export RUNASDAEMON

RUNNINGFILE=${WRKDIR}/${PROGBASENAME}.running
export RUNNINGFILE
fncheckrunning

linfo ""
linfo "Welcome to AppsOne Discovery Data Generator Script"
linfo "=================================================="
linfo ""

fnprchk $PREREQS

linfo "Running [${PROGNAME}]"
linfo "  Current Working Directory : [`pwd`]"
linfo "     Install Base Directory : [${BASEDIR}]"
linfo "    Configuration Directory : [${CFGDIR}]"
linfo "             Work Directory : [${WRKDIR}]"
linfo ""

linfo "Beginning AppsOne Discovery Data Generation..."
linfo ""

RUNASDAEMON=0
fnstartup
fnfinishup

}

fnredirmain $0 $* 2>&1 | tee ${SELFLOGFILE}
