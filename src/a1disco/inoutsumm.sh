#!/usr/bin/env bash

#
# Shell Scripting Array Variables
# -------------------------------
# - refer to element ${PORTBLACKLIST[<i>]}
# - length of element ${#PORTBLACKLIST[<i>]}
# - entire array: ${PORTBLACKLIST[@]}
# - when iterating do the followng (double-quotes are important)
#
#     for port in "${PORTBLACKLIST[@]}"
#     do
#       echo $port
#     done
#

declare -a PORTBLACKLIST
export PORTBLACKLIST
PORTBLACKLIST=(21 22 23)
assertPortIsBlacklisted()
{
  for port in "${PORTBLACKLIST[@]}"
  do
    if [ ${port} -eq $1 ]
    then
      #printf "assertPortIsBlacklisted(%d) returning 0 - is blacklisted\n" $1
      return 0
    fi
  done
  #printf "assertPortIsBlacklisted(%d) returning 1 - is NOT blacklisted\n" $1
  return 1
}

declare -a LOCALIPLIST
export LOCALIPLIST
assertIPAddressIsLocal()
{
  for localIP in "${LOCALIPLIST[@]}"
  do
    if [ "${localIP}" = "$1" ]
    then
      #printf "assertIPAddressIsLocal(%s) returning 0 - is local\n" "$1"
      return 0
    fi
  done
  #printf "assertIPAddressIsLocal(%s) returning 1 - is NOT local\n" "$1"
  return 1
}

printf "\n"
printf "Building list of Local IP Addresses\n"
printf "===================================\n"
printf "\n"

i=0
while read LOCALIP
do
  LOCALIPLIST[${i}]="${LOCALIP}"
  i=`expr ${i} \+ 1`
  printf "  %2d. %s\n" ${i} "${LOCALIP}"
done < <(grep -v "^#" ntts-nic.csv | cut -f4 -d',' | sort -n | uniq)

printf "\n"
printf "Inbound (Accepted-in) Connections - Client IP addresses\n"
printf "=======================================================\n"
printf "\n"

IDX=0
grep -v "^#" ntts-inandout.csv | cut -f1,2 -d',' | sort -n | uniq | while read line
do
  read -r SERVICEIP SERVICEPORT <<< $(echo ${line//,/ })
  assertIPAddressIsLocal "${SERVICEIP}" || continue
  assertPortIsBlacklisted ${SERVICEPORT} && continue
  IDX=`expr ${IDX} \+ 1`
  printf "  %2d. Local Service @ %15s : %-5s\n" ${IDX} ${SERVICEIP} ${SERVICEPORT}
  grep -v "^#" ntts-inandout.csv | grep "^${SERVICEIP},${SERVICEPORT}," | cut -f3 -d',' | sort -n | uniq -c | sed -e 's/^/      /g'
  printf "\n"
  printf "      Accepting programs are:\n"
  grep -v "^#" ntts-inandout.csv | grep "^${SERVICEIP},${SERVICEPORT}," | cut -f4,6 -d',' | sort -n | sed -e 's/^\([0-9]*\),\(.*\)$/PID: \1, PROG: \2/g' | uniq -c | sed -e 's/^/      /g'
  printf "\n"
done

printf "\n"
printf "Outbound (Connected-out) Connections - Client IP addresses\n"
printf "==========================================================\n"
printf "\n"
IDX=0
grep -v "^#" ntts-inandout.csv | cut -f1,2,3 -d',' | sort -n | uniq | while read line
do
  read -r SERVICEIP SERVICEPORT CLIENTIP <<< $(echo ${line//,/ })
  assertIPAddressIsLocal "${CLIENTIP}" || continue
  assertPortIsBlacklisted ${SERVICEPORT} && continue
  printf "${SERVICEIP},${SERVICEPORT},${CLIENTIP}\n"
done | sort -n | uniq | while read remoteServiceAddrAndClientIP
do
  read -r SERVICEIP SERVICEPORT CLIENTIP <<< $(echo ${remoteServiceAddrAndClientIP//,/ })
  IDX=`expr ${IDX} \+ 1`
  printf "  %2d. Remote Service @ %15s : %-5s (using local IP %s)\n" ${IDX} ${SERVICEIP} ${SERVICEPORT} ${CLIENTIP}
  grep -v "^#" ntts-inandout.csv | grep "^${SERVICEIP},${SERVICEPORT},${CLIENTIP}," | cut -f4,6 -d',' | sort -n | sed -e 's/^\([0-9]*\),\(.*\)$/PID: \1, PROG: \2/g' | uniq -c | sed -e 's/^/      /g'
  printf "\n"
done

printf "\n"
printf "Unique list of client-IP addresses in inbound/accept()ed connections\n"
printf "====================================================================\n"
printf "\n"
IDX=0
grep -v "^#" ntts-inandout.csv | cut -f1,2 -d',' | sort -n | uniq | while read line
do
  read -r SERVICEIP SERVICEPORT <<< $(echo ${line//,/ })
  assertIPAddressIsLocal "${SERVICEIP}" || continue
  assertPortIsBlacklisted ${SERVICEPORT} && continue
  grep -v "^#" ntts-inandout.csv | grep "^${SERVICEIP},${SERVICEPORT}," | cut -f3 -d','
done | sort -n | uniq -c | sed -e 's/^/      /g'
printf "\n"

printf "\n"
printf "Unique list of service-IP addresses in outbound/connect()ed connections\n"
printf "=======================================================================\n"
printf "\n"
grep -v "^#" ntts-inandout.csv | cut -f2,3 -d',' | sort -n | uniq | while read line
do
  read -r SERVICEPORT CLIENTIP <<< $(echo ${line//,/ })
  assertIPAddressIsLocal "${CLIENTIP}" || continue
  assertPortIsBlacklisted ${SERVICEPORT} && continue
  grep -v "^#" ntts-inandout.csv | grep "^[^,]*,${SERVICEPORT},${CLIENTIP}," | cut -f1 -d','
done | sort -n | uniq -c | sed -e 's/^/      /g'

