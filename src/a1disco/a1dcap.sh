#!/usr/bin/env bash

PROGNAME=$0
PROGBASENAME=`basename ${PROGNAME}`
BASEDIR=`dirname ${PROGNAME}`
LIBDIR=${BASEDIR}/lib
CFGDIR=${BASEDIR}/config
WRKDIR=${BASEDIR}/work


export BASEDIR
export LIBDIR
export CFGDIR
export WRKDIR

YYYYMMDDHHMISS=`date +"%Y%m%d%H%M%S"`
export YYYYMMDDHHMISS

SELFLOGFILE="${PROGBASENAME//.sh/}_${YYYYMMDDHHMISS}-outerr.log"
export SELFLOGFILE

fnmain()
{
linfo "Creating directory for storing captured raw data [${RAWCAPDIR}]..."
mkdir ${RAWCAPDIR}

linfo "Retrieving host details (uname -a, hostname, <UNIX OS flavour specific commands>) ..."

fnpfx
uname -a 1>${FNPREFIX}.uname_a.out 2>${FNPREFIX}.uname_a.err
hostname 1>${FNPREFIX}.hostname.out 2>${FNPREFIX}.hostname.err
fnhost

linfo "Retrieving list of network interfaces ..."
fnpfx
fnnet
if [ "x${LOWESTMACADDR}" = "x" ]
then
  LOWESTMACADDR="NOMACADDR"
fi
linfo "Generated unique host identity - ${LOWESTMACADDR} ..."

NEWCAPDIR=${RAWCAPDIR}_${LOWESTMACADDR}
mv ${RAWCAPDIR} ${NEWCAPDIR}
RAWCAPDIR=${NEWCAPDIR}
export RAWCAPDIR
linfo "Renamed raw data capture directory to [${RAWCAPDIR}]..."

linfo "Retrieving list of mounted storage devices ..."
fnpfx
fnmntdev

linfo "Retrieving list of active TCP addresses and processes ..."
fnpfx
fnprocaddr

linfo "Retrieving open-files listing for TCP-active processes ..."
fnpfx
fnprocpath

fnpfx
if [ -z "${SKIPOPENFILEDETAILS}" ]
then
  linfo "Retrieving details of open files of TCP-active processes [SKIPOPENFILEDETAILS not set]..."
  fnpath
else
  linfo "Skipping details of open files of TCP-active processes [SKIPOPENFILEDETAILS is set]..."
fi

fnrm0 ${RAWCAPDIR}
fntargzrm ${RAWCAPDIR}
}

fnredirmain()
{

. ${LIBDIR}/common/lib.sh
. ${LIBDIR}/common/logging.sh
fnuname

linfo "Loading common and platform specific shell function library modules ..."
for file in ${LIBDIR}/common/*.sh ${LIBDIR}/${UNAME}/*.sh
do
  linfo "  sourcing [${file}]..."
  . ${file}
done

trap 'fnsiginchild' SIGTERM SIGINT

PREREQS="which printf uname hostname awk cut cat grep egrep ls find mv rm sort uniq head tail netstat ifconfig lsof"
export PREREQS
fnaddprs

RUNASDAEMON=0
export RUNASDAEMON

RUNNINGFILE=${WRKDIR}/${PROGBASENAME//.sh/}.running
export RUNNINGFILE
fncheckrunning

linfo ""
linfo "Welcome to AppsOne Discovery Data Capture Script"
linfo "================================================"
linfo ""

fnprchk $PREREQS

linfo "Running [${PROGNAME}]"
linfo "  Current Working Directory : [`pwd`]"
linfo "     Install Base Directory : [${BASEDIR}]"
linfo "    Configuration Directory : [${CFGDIR}]"
linfo "             Work Directory : [${WRKDIR}]"
if [ ${RUNASDAEMON} -eq 1 ]
then
linfo "          Mode of execution : [run in loop as daemon]"
else
linfo "          Mode of execution : [run exactly once]"
fi
linfo ""

linfo "Beginning AppsOne Auto-Discovery Data Capture"
linfo ""

fnstartup
fnfinishup

}

fnredirmain $0 $* 2>&1 | tee ${SELFLOGFILE}
