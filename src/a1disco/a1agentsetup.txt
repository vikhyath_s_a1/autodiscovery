#
# Input - (sshHost [sshPort])+
#
# Default - a1UserId=appsone
# Default - a1IDRSA=appsone.idrsa
# Default - a1DiscoDistro=a1disco-vXY.tgz
# Default - a1DiscoDistrosDir=../distros
#
# For each (sshHost [sshPort])
#   Elicit userId (default root)
#   Record if userIdIsRoot
#   ssh -p ${sshPort} -o User=${userId} ${sshHost} <<EOF
#     # uname
#     # ${sudo} ${pre-requisites-checks}
#     # ${sudo} ${useradd-command}
#     # ${sudo} ${sudo-setup-commands}
#     #        # password-less sudo invocations
#     #        # for all required pre-requisites
#   EOF
#   fnsshplls ${a1IDRSA} ${sshHost} ${a1UserId} ${sshPort}
#   ssh-add ${a1IDRSA}
#   scp -i ${a1IDRSA} -P ${sshPort} -o User=${a1UserId} 
#       ${a1DiscoDistrosDir}/${a1DiscoDistro}
#       ${ssHost}:${HOME}
#   ssh -i ${a1IDRSA} -p ${sshPort} -o User=${a1UserId} ${sshHost} <<EOF
#     # tar -zxvf ${a1DiscoDistro}
#     # cd ${a1DiscoDistro//-v??.tgz/}
#     # cd work
#     # ../a1dcap.sh
#   EOF 
#   scp -i ${a1IDRSA} -P ${sshPort} -o User=${a1UserId}
#       ${sshHost}:${HOME}/${a1DiscoDistro}/work/a1dcap_*.log
#       .
#   scp -i ${a1IDRSA} -P ${sshPort} -o User=${a1UserId}
#       ${sshHost}:${HOME}/${a1DiscoDistro}/work/a1dcap_*.tgz
#       .
#   tar -zxvf a1dcap_*.tgz
#   cd a1dcap_*_*
#   ../../a1dgen.sh
#   echo "the XLSX file that has it all"
#   Clean up...
#     rm -rvf a1dcap_*
#
