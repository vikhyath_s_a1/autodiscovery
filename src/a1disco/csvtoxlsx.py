#!/opt/local/bin/python

import sys
import csv
from xlsxwriter.workbook import Workbook

if (len(sys.argv) < 3):
    print ("Usage : %s <output-xlsx-filename> <csv1> [ <csv2> ... ]" %
           (sys.argv[0]))
    sys.exit(-1)

xlsx = sys.argv[1]
print ("Creating XLSX workbook named '%s'" % (xlsx))
workbook = Workbook(xlsx)

for csvfile in sys.argv[2:]:
    print ("Adding worksheet '%s'" % (csvfile))
    worksheet = workbook.add_worksheet (csvfile)

    with open (csvfile, 'r') as csvf:
        reader = csv.reader(csvf)
        for r, row in enumerate(reader):
            for c, col in enumerate(row):
                #write the csv file content into it
                worksheet.write(r, c, col.decode("utf-8"))

print ("Closing XLSX workbook")
workbook.close()

sys.exit(0)
