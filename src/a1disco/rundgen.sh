#!/usr/bin/env bash

PROGNAME=rundgen
export PROGNAME

a1discoDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo "${PROGNAME}: A1DISCO directory is [${a1discoDir}]..."

for dcapDir in $*
do
  echo "${PROGNAME}: Processing A1DCAP dump directory: [${dcapDir}]..."
  cd $dcapDir
  echo "${PROGNAME}:   Running [${a1discoDir}/a1dgen.sh] in it..."
  ${a1discoDir}/a1dgen.sh 2>&1 >/dev/null
  xc=$?
  if [ ${xc} -ne 0 ]
  then
    echo "${PROGNAME}:     - Exit code is non-ZERO [${xc}]"
    continue
  fi
  echo "${PROGNAME}:   Running [${a1discoDir}/inoutsumm.sh] in it..."
  ${a1discoDir}/a1dgen.sh 2>&1
  cd -
done

echo
echo "${PROGNAME}: Done dona done done"
echo
