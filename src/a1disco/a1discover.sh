#!/usr/bin/env bash

#
# Input - (sshHost sshUser [sshPort])+
#
# Default - sshPort - 22
# Default - a1DiscoDistroName=a1disco-vXY
# Default - a1DiscoDistroExtn=.tgz
# Default - a1DiscoDistrosDir=../distros
# Default - ${SELF
#
# Check and ensure that ${sshUser} already has ${a1DiscoDistroName} in ${HOME}
#   ALREADYPRESENT=`ssh -p ${sshPort} -o User=${sshUser} ${sshHost} <<EOF
#     A1DCAP=${a1DiscoDistroName}/a1dcap.sh
#     [ -x ${A1DCAP} ] && echo "1" || echo "0"
#     EOF`
#   while [ ${ALREADYPRESENT} -ne 1 ]
#   do
#     # Put in the distro
#     scp -P ${sshPort} -o User=${sshUser} \
#       ${a1DiscoDistroDir}/${a1DiscoDistroName}${a1DiscoDistroExtn} \
#       ${sshHost}:~
#     ALREADYPRESENT=`ssh -p ${sshPort} -o User=${sshUser} ${sshHost} <<EOF
#       tar -zxvf ${a1DiscoDistroName}${a1DiscoDistroExtn}
#       A1DCAP=${a1DiscoDistroName}/a1dcap.sh
#       [ -x ${A1DCAP} ] && echo "1" || echo "0"
#       EOF`
#   done
#
# Invoke a1dcap.sh
#   # - Allow regular output to get captured in this scripts ${SELFLOGFILE}.
#   # - At the same time, figure out the ${SELFLOGFILE} base-name of the
#   #   invoked a1dcap.sh.
#   CBNFILE=a1dcap.sh.cbn.tmp
#   ssh -p ${sshPort} -o User=${sshUser} ${sshHost} <<EOF
#     cd ${a1DiscoDistroName}/work
#     rm -rf *
#     ../a1dcap.sh
#     EOF | awk '
#       {
#         print $0;
#         if ($0 ~ / Terminal output has been saved in /) {
#           cbn = $0;
#           sub (/^.* Terminal output has been saved in /, "", cbn);
#           sub (/-outerr.log$/, "", cbn);
#           printf ("%s", cbn) > '"${CBNFILE}"';
#         }
#       }'
#     CBN=`cat ${CBNFILE}`
#
# Transfer invocation log and capture dump archive over
#   scp -P ${sshPort} -o User=${sshUser} \
#     ${sshHost}:~/${a1DiscoDistroName}/work/${CBN}-outerr.log \
#     ${sshHost}:~/${a1DiscoDistroName}/work/${CBN}_*.tgz \
#     .
#
# Remove the files from the target host
#   ssh -p ${sshPort} -o User=${sshUser} ${sshHost} <<EOF
#     cd ${a1DiscoDistroName}/work
#     rm -rf *
#     EOF
#
# Extract the capture dumps
#   tar -zxvf ${CBN}_*.tgz
#   cd ${CBN}_*
#   ../../a1dgen.sh
#

if [ $# -lt 2 ]
then
  echo
  echo "  Usage : $0 <host> <user> [<port>]"
  echo
fi

ARGS_SSH_HOST=
ARGS_SSH_USER=
ARGS_SSH_PORT=22

ARGS_SSH_HOST=$1; shift
ARGS_SSH_USER=$1; shift
[$# -gt 0 ] && ARGS_SSH_PORT=$1; shift

PROGNAME=$0
PROGBASENAME=`basename ${PROGNAME}`
BASEDIR=`dirname ${PROGNAME}`
LIBDIR=${BASEDIR}/lib
CFGDIR=${BASEDIR}/config
WRKDIR=${BASEDIR}/work
CMPDIR=${BASEDIR}/components

export BASEDIR
export LIBDIR
export CFGDIR
export WRKDIR

YYYYMMDDHHMISS=`date +"%Y%m%d%H%M%S"`
export YYYYMMDDHHMISS

SELFLOGFILE="${PROGBASENAME//.sh/}_${YYYYMMDDHHMISS}-outerr.log"
export SELFLOGFILE

fnmain()
{

echo "Commencing AppsOne Discovery on ${ARGS_SSH_USER} @ ${ARGS_SSH_HOST} : ${ARGS_SSH_PORT}"

ssh -p ${ARGS_SSH_PORT} -o User=${ARGS_SSH_USER} ${ARGS_SSH_HOST} <<a1dcap.sh

}

fnredirmain()
{

UNAME=`uname`
export UNAME
. ${LIBDIR}/common/lib.sh
. ${LIBDIR}/common/logging.sh
linfo "Loading common and platform specific shell function library modules ..."
for file in ${LIBDIR}/common/*.sh ${LIBDIR}/${UNAME}/*.sh
do
  linfo "  sourcing [${file}]..."
  . ${file}
done

trap 'fnsiginchild' SIGTERM SIGINT

PREREQS="which printf uname hostname awk cut cat grep egrep ls find mv rm sort uniq head tail netstat ifconfig lsof"
export PREREQS
fnaddprs

RUNASDAEMON=0
export RUNASDAEMON

RUNNINGFILE=${WRKDIR}/${PROGBASENAME}.running
export RUNNINGFILE
fncheckrunning

linfo ""
linfo "Welcome to AppsOne Discovery Automation script"
linfo "=============================================="
linfo ""

fnprchk $PREREQS

linfo "Running [${PROGNAME}]"
linfo "  Current Working Directory : [`pwd`]"
linfo "     Install Base Directory : [${BASEDIR}]"
linfo "    Configuration Directory : [${CFGDIR}]"
linfo "             Work Directory : [${WRKDIR}]"
linfo ""

linfo "Beginning AppsOne Discovery Automation..."
linfo ""

RUNASDAEMON=0
fnstartup
fnfinishup

}

fnredirmain $0 $* 2>&1 | tee ${SELFLOGFILE}
