#!/bin/bash

fn_NIC_csv()
{
  if [ ! -f "002.netstat_i.out" ]
  then
    lerror "No file named [002.netstat_i.out]! Cannot create (:NIC)s"
    return 1
  fi

  LOCALIPS_FILENAME=${MARKUP_PREFIX}localips.${MARKUP_SUFFIX}
  printf "#ipv,ip,nic\n" >${LOCALIPS_FILENAME}

  printf "#nic,ipv,ipcount,ipaddrs,flags-str\n"

  tail -n +2 002.netstat_i.out | awk '{print $1;}' | sort | uniq | while read iname
  do
    if [ ! -f "002.ifconfig_${iname}.out" ]
    then
      lerror "No file named [002.ifconfig_${iname}.out]! Cannot create (:NIC) for '${iname}'"
      continue;
    fi
    cat 002.ifconfig_${iname}.out | awk '
      BEGIN {
        nic="'${iname}'";
        flags="";
        ip4count=0;
        ip4addrs="";
        ip6count=0;
        ip6addrs="";
      }
      /^'${iname}': flags=/ {
        flags = $0;
        sub (/^'${iname}': flags=/, "", flags);
      }
      /^[ \t][ \t]*inet[ \t]/ {
        ip4addr = $0;
        sub (/^[ \t][ \t]*inet[ \t]*/, "", ip4addr);
        sub (/[ \t].*$/, "", ip4addr);
        if (0 == ip4count) {
          ip4addrs = ip4addr;
        } else {
          ip4addrs = (ip4addrs " " ip4addr);
        }
        ip4count = ip4count + 1;
        printf ("%s,%s,%s\n", "IPv4", ip4addr, "'${iname}'") >> "'${LOCALIPS_FILENAME}'";
      }
      /^[ \t][ \t]*inet6[ \t]/ {
        ip6addr = $0;
        sub (/^[ \t][ \t]*inet6[ \t]*/, "", ip6addr);
        sub (/[ \t].*$/, "", ip6addr);
        sub (/%.*$/, "", ip6addr);
        if (0 == ip6count) {
          ip6addrs = ip6addr;
        } else {
          ip6addrs = (ip6addrs " " ip6addr);
        }
        ip6count = ip6count + 1;
        printf ("%s,%s,%s\n", "IPv6", ip6addr, "'${iname}'") >> "'${LOCALIPS_FILENAME}'";
      }
      END {
        #printf ("%s,%s\n", "'${iname}'",flags);
        if (0 < ip4count)
          printf ("%s,%s,%d,%s,%s\n",
              "'${iname}'", "IPv4", ip4count, ip4addrs, flags);
        if (0 < ip6count)
          printf ("%s,%s,%d,%s,%s\n",
              "'${iname}'", "IPv6", ip6count, ip6addrs, flags);
      }'
  done

  fn_print_output_filenames ${LOCALIPS_FILENAME}
}

fn_NIC_cql()
{
  if [ ! -f "002.netstat_i.out" ]
  then
    lerror "No file named [002.netstat_i.out]! Cannot create (:NIC)s"
    return 1
  fi

  LOCALIPS_FILENAME=${MARKUP_PREFIX}localips.csv
  printf "#ipv,ip,nic\n" >${LOCALIPS_FILENAME}

  tail -n +2 002.netstat_i.out | awk '{print $1;}' | sort | uniq | while read iname
  do
    if [ ! -f "002.ifconfig_${iname}.out" ]
    then
      lerror "No file named [002.ifconfig_${iname}.out]! Cannot create (:NIC) for '${iname}'"
      continue;
    fi
    printf "node (:NIC {\n"
    printf "  %*s : \"%s\",\n" ${PROPNAMEWIDTH} "d_nic_name" "${iname}"
    cat 002.ifconfig_${iname}.out | awk '
      BEGIN {numIPs=0;}
      /^'${iname}': flags=/ {
        flags = $0;
        sub (/^'${iname}': flags=/, "", flags);
        printf ("  %*s : \"%s\",\n", '${PROPNAMEWIDTH}', "d_AIX_flags", flags);
      }
      /^[ \t][ \t]*inet[ \t]/ {
        ip4addr = $0;
        sub (/^[ \t][ \t]*inet[ \t]*/, "", ip4addr);
        sub (/[ \t].*$/, "", ip4addr);
        printf ("  %*s : \"%s\",\n", '${PROPNAMEWIDTH}', ("d_ip4addr_" (++numIPs)), ip4addr);
        printf ("%s,%s,%s\n", "IPv4", ip4addr, "'${iname}'") >> "'${LOCALIPS_FILENAME}'";
      }
      /^[ \t][ \t]*inet6[ \t]/ {
        ip6addr = $0;
        sub (/^[ \t][ \t]*inet6[ \t]*/, "", ip6addr);
        sub (/[ \t].*$/, "", ip6addr);
        sub (/%.*$/, "", ip6addr);
        printf ("  %*s : \"%s\",\n", '${PROPNAMEWIDTH}', ("d_ip6addr_" (++numIPs)), ip6addr);
        printf ("%s,%s,%s\n", "IPv6", ip6addr, "'${iname}'") >> "'${LOCALIPS_FILENAME}'";
      }
      END {
        printf ("  %*s : %d,\n", '${PROPNAMEWIDTH}', "d_ipaddr_count", numIPs);
      }'
    printf "  %*s : \"%s\"\n" ${PROPNAMEWIDTH} "d_observation_ts" "${OBSRVTS}"
    printf "})\n"
    printf "\n"
  done

  tail -n +2 ${LOCALIPS_FILENAME} | awk '{
    ip46 = $1;
    ipv = $2;
    nic = $3;
    printf ("node (:IPADDRESS {\n");
    printf ("  %*s : \"%s\",\n", '${PROPNAMEWIDTH}', "d_ip_address", ip46);
    printf ("  %*s : \"%s\",\n", '${PROPNAMEWIDTH}', "d_ip_version", ipv);
    printf ("  %*s : \"%s\"\n",  '${PROPNAMEWIDTH}', "d_nic_name", nic);
    printf ("})\n\n");
  }'

  fn_print_output_filenames ${LOCALIPS_FILENAME}
}
