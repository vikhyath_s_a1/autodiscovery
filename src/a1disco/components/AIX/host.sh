#!/bin/bash


fn_HOST_oslevel_csv()
{
  printf "%s,%s\n" "d_${UNAME}_oslevel" "`cat 001.oslevel.out`"
}

fn_HOST_lparstat_i_csv()
{
  cat 001.lparstat_i.out | awk -F ':' '
    /: \-$/ {next;}
    {
      captureParam = 0;
      if ($1 ~ /Virtual CPUs/)
        captureParam = 1;
      if ($1 ~ /Physical CPUs/)
        captureParam = 1;
      if ($1 ~ /Memory/)
        captureParam = 1;
      if (captureParam) {
        name = $1;
        gsub (/  *$/, "", name);
        gsub (/^  */, "", name);
        gsub (/  */, "_", name);
        gsub (/^/, "d_'${UNAME}'_lparstat_i_", name);
  
        value = $2;
        gsub (/  *$/, "", value);
        gsub (/^  */, "", value);
  
        printf ("%s,%s\n", name, value);
      }
    }'
  #printf "  %38s : \"%s\",\n" "d_${UNAME}_lparstat_i" "`cat $1`"
}
fn_HOST_oslevel_cql ()
{
  printf "  %*s : \"%s\",\n" ${PROPNAMEWIDTH} "d_${UNAME}_oslevel" "`cat 001.oslevel.out`"
}

fn_HOST_lparstat_i_cql()
{
  cat 001.lparstat_i.out | awk -F ':' '
    /: \-$/ {next;}
    {
      captureParam = 0;
      if ($1 ~ /Virtual CPUs/)
        captureParam = 1;
      if ($1 ~ /Physical CPUs/)
        captureParam = 1;
      if ($1 ~ /Memory/)
        captureParam = 1;
      if (captureParam) {
        name = $1;
        gsub (/  *$/, "", name);
        gsub (/^  */, "", name);
        gsub (/  */, "_", name);
        gsub (/^/, "d_'${UNAME}'_lparstat_i_", name);
  
        value = $2;
        gsub (/  *$/, "", value);
        gsub (/^  */, "", value);
  
        printf ("  %*s : \"%s\",\n", '${PROPNAMEWIDTH}', name, value);
      }
    }'
  #printf "  %38s : \"%s\",\n" "d_${UNAME}_lparstat_i" "`cat $1`"
}

