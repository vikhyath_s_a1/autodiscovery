#!/bin/bash


fn_MNTDEV_csv()
{
  if [ ! -f "003.mount.out" ]
  then
    lerror "No file named [003.mount.out]! Cannot create (:MNTDEV)s"
    return 1
  fi

  printf "#device,mount-point,file-system,nfs-node\n"

  tail +3 003.mount.out | awk '
    /^[ \t]/ { # NFS are not there
      nfsNode = "";
      devFsPath = $1;
      mountPoint = $2;
      fileSystem = $3;
    }
    /^[^ \t]/ { # NFS are there
      nfsNode = $1;
      devFsPath = $2;
      mountPoint = $3;
      fileSystem = $4;
    }
    {
      printf ("%s,%s,%s,%s\n",
        devFsPath, mountPoint, fileSystem, nfsNode);
    }
    ' 
  fn_print_output_filenames
}

fn_MNTDEV_cql()
{
  if [ ! -f "003.mount.out" ]
  then
    lerror "No file named [003.mount.out]! Cannot create (:MNTDEV)s"
    return 1
  fi
  tail +3 003.mount.out | awk '
    /^[ \t]/ { # NFS are not there
      nfsNode = "";
      devFsPath = $1;
      mountPoint = $2;
      fileSystem = $3;
    }
    /^[^ \t]/ { # NFS are there
      nfsNode = $1;
      devFsPath = $2;
      mountPoint = $3;
      fileSystem = $4;
    }
    {
        printf ("node (:MNTDEV {\n");
        printf ("  %*s : \"%s\",\n", '${PROPNAMEWIDTH}', "d_dev_path", devFsPath);
        printf ("  %*s : \"%s\",\n", '${PROPNAMEWIDTH}', "d_mount_point", mountPoint);
        printf ("  %*s : \"%s\",\n", '${PROPNAMEWIDTH}', "d_file_system", fileSystem);
        printf ("  %*s : \"%s\",\n", '${PROPNAMEWIDTH}', "d_nfs_node", nfsNode);
        printf ("  %*s : \"%s\"\n",  '${PROPNAMEWIDTH}', "d_observation_ts", '${OBSRVTS}');
        printf ("})\n");
        printf ("\n");
    }
    ' 
  fn_print_output_filenames
}
