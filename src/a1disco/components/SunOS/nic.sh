#!/bin/bash

fn_NIC_csv()
{
  if [ ! -f "002.ifconfig_a.out" ]
  then
    lerror "No file named [002.ifconfig_a.out]! Cannot create (:NIC)s"
    return 1
  fi

  LOCALIPS_FILENAME=${MARKUP_PREFIX}localips.${MARKUP_SUFFIX}
  printf "#ipv,ip,nic\n" >${LOCALIPS_FILENAME}

  printf "#nic,ipv,ipcount,ipaddrs,flags-str\n"

  cat 002.ifconfig_a.out | awk '
    BEGIN {
      nic="";
      flags="";
      ip4count=0;
      ip4addrs="";
      ip6count=0;
      ip6addrs="";
    }
    /^.*: flags=/ {
      if (0 < ip4count) {
        printf ("%s,%s,%d,%s,%s\n", nic, "IPv4", ip4count, ip4addrs, flags);
      }
      if (0 < ip6count) {
        printf ("%s,%s,%d,%s,%s\n", nic, "IPv6", ip6count, ip6addrs, flags);
      }
      nic = $0;
      gsub (/:.*$/, "", nic);
      flags = $0;
      gsub (/^.*: flags=/, "", flags);
      ip4count=0;
      ip4addrs="";
      ip6count=0;
      ip6addrs="";
    }
    /^[ \t][ \t]*inet[ \t]/ {
      if (nic == "")
        next;
      ip4addr = $0;
      gsub (/^[ \t][ \t]*inet[ \t]*/, "", ip4addr);
      gsub (/[ \t].*$/, "", ip4addr);
      if (0 == ip4count) {
        ip4addrs = ip4addr;
      } else {
        ip4addrs = (ip4addrs " " ip4addr);
      }
      ip4count = ip4count + 1;
      printf ("%s,%s,%s\n", "IPv4", ip4addr, nic) >> "'${LOCALIPS_FILENAME}'";
    }
    /^[ \t][ \t]*inet6[ \t]/ {
      if (nic == "")
        next;
      ip6addr = $0;
      gsub (/^[ \t][ \t]*inet6[ \t]*/, "", ip6addr);
      gsub (/[ \t].*$/, "", ip6addr);
      gsub (/%.*$/, "", ip6addr);
      if (0 == ip6count) {
        ip6addrs = ip6addr;
      } else {
        ip6addrs = (ip6addrs " " ip6addr);
      }
      ip6count = ip6count + 1;
      printf ("%s,%s,%s\n", "IPv6", ip6addr, nic) >> "'${LOCALIPS_FILENAME}'";
    }
    END {
      #printf ("%s,%s\n", "'${iname}'",flags);
      if (0 < ip4count) {
        printf ("%s,%s,%d,%s,%s\n", nic, "IPv4", ip4count, ip4addrs, flags);
      }
      if (0 < ip6count) {
        printf ("%s,%s,%d,%s,%s\n", nic, "IPv6", ip6count, ip6addrs, flags);
      }
    }'

  fn_print_output_filenames ${LOCALIPS_FILENAME}
}

