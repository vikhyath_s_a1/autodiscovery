#!/bin/bash


fn_HOST_sw_vers_csv()
{
  cat 001.sw_vers.out | awk '
    {
      captureParam = 1;
      if (captureParam) {
        name = $1;
        gsub (/\t/, " ", name);
        gsub (/  *$/, "", name);
        gsub (/^  */, "", name);
        gsub (/  */, "_", name);
        gsub (/:/, "_", name);
        gsub (/^/, "d_'${UNAME}'_sw_vers_", name);
  
        value = $2;
        gsub (/  *$/, "", value);
        gsub (/^  */, "", value);
  
        printf ("%s,%s\n", name, value);
      }
    }'
}

fn_HOST_system_profiler_detailLevel_basic_csv()
{
  cat 001.system_profiler_detailLevel_basic.out | awk -F ':' '
    /:[ \t]*$/ {next;}
    {
      captureParam = 0;
      if ($1 ~ /Processor/)
        captureParam = 1;
      if ($1 ~ /Memory/)
        captureParam = 1;
      if (captureParam) {
        name = $1;
        gsub (/\t/, " ", name);
        gsub (/  *$/, "", name);
        gsub (/^  */, "", name);
        gsub (/  */, "_", name);
        gsub (/:/, "_", name);
        gsub (/^/, "d_'${UNAME}'_system_profiler_detailLevel_basic_", name);
  
        value = $2;
        gsub (/  *$/, "", value);
        gsub (/^  */, "", value);
  
        printf ("%s,%s\n", name, value);
      }
    }'
}

