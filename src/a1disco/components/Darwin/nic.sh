#!/bin/bash

fn_NIC_csv()
{
  if [ ! -f "002.ifconfig_l.out" ]
  then
    lerror "No file named [002.ifconfig_l.out]! Cannot create (:NIC)s"
    return 1
  fi

  LOCALIPS_FILENAME=${MARKUP_PREFIX}localips.${MARKUP_SUFFIX}
  printf "#ipv,ip,nic\n" >${LOCALIPS_FILENAME}

  printf "#nic,ipv,ipcount,ipaddrs,flags-str\n"

  for iname in `cat 002.ifconfig_l.out`
  do
    if [ ! -f "002.ifconfig_${iname}.out" ]
    then
      lerror "No file named [002.ifconfig_${iname}.out]! Cannot create (:NIC) for '${iname}'"
      continue;
    fi
    cat 002.ifconfig_${iname}.out | awk '
      BEGIN {
        nic="'${iname}'";
        flags="";
        ip4count=0;
        ip4addrs="";
        ip6count=0;
        ip6addrs="";
      }
      /^'${iname}': flags=/ {
        flags = $0;
        sub (/^'${iname}': flags=/, "", flags);
      }
      /^[ \t][ \t]*inet[ \t]/ {
        ip4addr = $0;
        sub (/^[ \t][ \t]*inet[ \t]*/, "", ip4addr);
        sub (/[ \t].*$/, "", ip4addr);
        if (0 == ip4count) {
          ip4addrs = ip4addr;
        } else {
          ip4addrs = (ip4addrs " " ip4addr);
        }
        ip4count = ip4count + 1;
        printf ("%s,%s,%s\n", "IPv4", ip4addr, "'${iname}'") >> "'${LOCALIPS_FILENAME}'";
      }
      /^[ \t][ \t]*inet6[ \t]/ {
        ip6addr = $0;
        sub (/^[ \t][ \t]*inet6[ \t]*/, "", ip6addr);
        sub (/[ \t].*$/, "", ip6addr);
        sub (/%.*$/, "", ip6addr);
        if (0 == ip6count) {
          ip6addrs = ip6addr;
        } else {
          ip6addrs = (ip6addrs " " ip6addr);
        }
        ip6count = ip6count + 1;
        printf ("%s,%s,%s\n", "IPv6", ip6addr, "'${iname}'") >> "'${LOCALIPS_FILENAME}'";
      }
      END {
        #printf ("%s,%s\n", "'${iname}'",flags);
        if (0 < ip4count)
          printf ("%s,%s,%d,%s,%s\n",
              "'${iname}'", "IPv4", ip4count, ip4addrs, flags);
        if (0 < ip6count)
          printf ("%s,%s,%d,%s,%s\n",
              "'${iname}'", "IPv6", ip6count, ip6addrs, flags);
      }'
  done

  fn_print_output_filenames ${LOCALIPS_FILENAME}
}

