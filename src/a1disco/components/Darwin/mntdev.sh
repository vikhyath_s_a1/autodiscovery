#!/bin/bash

fn_MNTDEV_csv()
{
  if [ ! -f "003.mount.out" ]
  then
    lerror "No file named [003.mount.out]! Cannot create (:MNTDEV)s"
    return 1
  fi

  printf "#device,mount-point,file-system,nfs-node\n"
  cat 003.mount.out | sed -e 's/\(.*\) on \(.*\) (\([^,]*\),.*)$/\1,\2,\3,/g'

  fn_print_output_filenames
}
