#!/bin/bash

fn_PROCESS_ADDRESS_csv()
{
  if [ ! -f "004.lsof_i.out" ]
  then
    lerror "No file named [004.lsof_i.out]! Cannot create (:PROCESS)es, $(:IPADDRESS)es, $(:SOCKADDRESS)es..."
    return 1
  fi

  PROCESSES_FILENAME=${MARKUP_PREFIX}processes.${MARKUP_SUFFIX}
  printf "#pid,parent-pid,effective-user,real-user,elapsed-time,cpu-time,progname,command-line\n" >${PROCESSES_FILENAME}

  linfo "Creating listen-sockets and  connections..."

  cat 004.ps_e_o.out | awk '
    BEGIN {
      grab = 0;
      chunk = 10;
      count = 0;
    }
    /^### / {
      pid = $0;
      gsub (/^### ps -e -o .* for  */, "", pid);
      gsub (/ *$/, "", pid);
      grab = 1;
      next;
    }
    (1 == grab) {
      grab = 0;
      if ($1 ~ pid) {
        printf ("%s,%s,%s,%s,%s,%s,%s",
          $1, $2, $3, $5, $7, $8, $9) >> "'${PROCESSES_FILENAME}'";
        $1=$2=$3=$4=$5=$6=$7=$8=$9="";
        gsub (/^[ \t][ \t]*/, "", $0);
        gsub (/[ \t][ \t]*$/, "", $0);
        printf (",%s\n", $0) >> "'${PROCESSES_FILENAME}'";
        count = count + 1;
        if (0 == count%chunk) {
          printf ("[INFO ]   ... created %d processes ...\n", count);
        }
      }
    }
    END {
      printf ("[INFO ]   Total [%d] processes created\n", count);
    }'
    
  LISTENSOCKS_FILENAME=${MARKUP_PREFIX}listensocks.${MARKUP_SUFFIX}
  printf "#cmd,pid,ipv,ip,port\n" >${LISTENSOCKS_FILENAME}

  CONNECTIONS_FILENAME=${MARKUP_PREFIX}connections.${MARKUP_SUFFIX}
  printf "#cmd,pid,ipv,localip,localport,remoteip,remoteport\n" >${CONNECTIONS_FILENAME}

  linfo "Creating listen-sockets and  connections..."

  cat 004.lsof_i.out | awk '
    BEGIN {
      lchunk = 100;
      lcount = 0;
      cchunk = 100;
      ccount = 0;
    }
    $8 ~ /^TCP$/ {
      cmd = $1;
      pid = $2;
      ipv = $5;
      dispo = $10;
  
      if (dispo == "(LISTEN)") {
        ipaddr = $9;
        sub (/:[0-9]*$/, "", ipaddr);
        gsub (/[\[\]]/, "", ipaddr);
  
        portnum = $9;
        gsub (/^.*:/, "", portnum);
  
        printf ("%s,%d,%s,%s,%d\n",
          cmd, pid, ipv, ipaddr, portnum) >> "'${LISTENSOCKS_FILENAME}'";
        lcount = lcount + 1;
        if (0 == lcount%lchunk) {
          printf ("[INFO ]   ... created %d listen-sockets ...\n", lcount);
        }
  
      } else
      if (dispo == "(ESTABLISHED)") {
        locaddr = $9;
        sub (/->.*$/, "", locaddr);
        locip = locaddr;
        sub (/:[0-9]*$/, "", locip);
        gsub (/[\[\]]/, "", locip);
        locport = locaddr;
        gsub (/^.*:/, "", locport);
  
        remaddr = $9;
        sub (/^.*->/, "", remaddr);
        remip = remaddr;
        sub (/:[0-9]*$/, "", remip);
        gsub (/[\[\]]/, "", remip);
        remport = remaddr;
        gsub (/^.*:/, "", remport);
  
        printf ("%s,%d,%s,%s,%d,%s,%d\n",
          cmd, pid, ipv, locip, locport, remip, remport) >> "'${CONNECTIONS_FILENAME}'";
        ccount = ccount + 1;
        if (0 == ccount%cchunk) {
          printf ("[INFO ]   ... created %d connections ...\n", ccount);
        }
      }
    }
    END {
      printf ("[INFO ]   Total [%d] listen-sockets created\n", lcount);
      printf ("[INFO ]   Total [%d] connections created\n", ccount);
    }'

  LOCALIPS_FILENAME=${MARKUP_PREFIX}localips.${MARKUP_SUFFIX}

  INANDOUT_FILENAME=${MARKUP_PREFIX}inandout.${MARKUP_SUFFIX}
  printf "#service-ip,service-port,client-ip,process-id,ip-version,program-name,isTheServerFlag(0/1)\n" >${INANDOUT_FILENAME}

  linfo "Creating in-and-out connection set..."

  INBOUNDCHUNK=100
  INBOUNDCOUNT=0
  OUTBOUNDCHUNK=10
  OUTBOUNDCOUNT=0

  tail -n +2 ${CONNECTIONS_FILENAME} | tr -s ',' ' ' | while read cmd pid ipv locip locport remip remport
  do
    INOROUT=
    grep -q ",${locport}$" ${LISTENSOCKS_FILENAME}
    if [ $? -eq 0 ]
    then
      SERVICEIP=${locip}
      SERVICEPORT=${locport}
      CLIENTIP=${remip}
      ACCEPTED=1
      INBOUNDCOUNT=`expr ${INBOUNDCOUNT} + 1`
      INOROUT=0
    else
      SERVICEIP=${remip}
      SERVICEPORT=${remport}
      CLIENTIP=${locip}
      ACCEPTED=0
      OUTBOUNDCOUNT=`expr ${OUTBOUNDCOUNT} + 1`
      INOROUT=1
    fi
    PROGNAME=`grep "^${pid}," ${PROCESSES_FILENAME} | cut -f7 -d','`
    if [ $? -ne 0 ]
    then
      lerror "Program name for process-id [${pid}] was not found in [${PROCESSES_FILENAME}]"
      continue;
    fi
    printf "${SERVICEIP},${SERVICEPORT},${CLIENTIP},${pid},${ipv},${PROGNAME},${ACCEPTED}\n" >>${INANDOUT_FILENAME}
    if [ 0 -eq ${INOROUT} ]
    then
      if [ 0 -eq `expr ${INBOUNDCOUNT} % ${INBOUNDCHUNK}` ]
      then
        linfo "  ... created %d inbound connections ..." ${INBOUNDCOUNT}
      fi
    else
      if [ 0 -eq `expr ${OUTBOUNDCOUNT} % ${OUTBOUNDCHUNK}` ]
      then
        linfo "  ... created %d outbound connections ..." ${OUTBOUNDCOUNT}
      fi
    fi
  done
  linfo "  Total [%d] inbound connections created"  `grep "1$" ${INANDOUT_FILENAME}|wc -l`
  linfo "  Total [%d] outbound connections created" `grep "0$" ${INANDOUT_FILENAME}|wc -l`

  fn_print_output_filenames \
    ${PROCESSES_FILENAME} \
    ${LISTENSOCKS_FILENAME} \
    ${CONNECTIONS_FILENAME} \
    ${INANDOUT_FILENAME}
}
