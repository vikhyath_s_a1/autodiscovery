#!/bin/bash

fn_print_output_filenames()
{
  linfo "Output written to following files -"
  for fn in ${MARKUP_FILENAME} $*
  do
    linfo "  [${fn}]"
  done
}

fn_LABEL_SUFFIX()
{
  if [ $# -eq 0 ]
  then
    lerror "Invalid Arguments! Usage: fn_LABEL_ <label> <suffix>"
    lerror "  <label> - one of the following:"
    lerror "    (HOST, NIC, MNTDEV, PROCESS_ADDRESS, PROCESS_PATH)"
    lerror "  <suffix> - one of the following:"
    lerror "    (cql, csv)"
    return 1
  fi

  LABEL_UPPER=`echo $1|tr '[a-z]' '[A-Z]'`
  LABEL_LOWER=`echo $1|tr '[A-Z]' '[a-z]'`
  shift
  if [ $# -eq 1 ]
  then
    MARKUP_SUFFIX=$1
  fi
  MARKUP_FILENAME="${MARKUP_PREFIX}${LABEL_LOWER//_/-}.${MARKUP_SUFFIX}"
  MARKUP_FUNCTION="fn_${LABEL_UPPER}_${MARKUP_SUFFIX}"
  rm -f ${MARKUP_FILENAME}

  declare -fF ${MARKUP_FUNCTION} >/dev/null
  if [ $? -ne 0 ]
  then
    lerror "No function named ${MARKUP_FUNCTION}!"
    return 1
  fi
  printf "#################################################################################\n"
  printf "#         0         0         0         0         0         0         0         0\n"
  ${MARKUP_FUNCTION} | awk ' {
    if ($0 ~ /^\[[A-Z ]*\] /) {
      printf ("%s\n", $0);
    } else {
      printf ("# %s\n", $0);
      printf ("%s\n", $0) >> "'${MARKUP_FILENAME}'";
    }
  }'
  printf "#         0         0         0         0         0         0         0         0\n"
  printf "#################################################################################\n"
}
