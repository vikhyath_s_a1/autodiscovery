fn_PROCESS_PATH_csv()
{
  if [ ! -f "005.lsof_p.out" ]
  then
    lerror "No file named [005.lsof_p.out]! Cannot create (:PATH)s"
    return 1
  else
    PROCINODES_FILENAME=${MARKUP_PREFIX}proc-inode.${MARKUP_SUFFIX}
    printf "#pid,fd,type,inode,device\n" >${PROCINODES_FILENAME}

    cat 005.lsof_p.out | grep -v "^#" | grep -v "^COMMAND " | awk '
      $8 ~ /^TCP$/ {next;}
      $8 ~ /^UDP$/ {next;}
      {
        pid = $2;
        fd = $4;
        type = $5;
        device = $6;
        inode = $8;
        printf ("%s,%s,%s,%s,%s\n", pid, fd, type, inode, device) >> "'${PROCINODES_FILENAME}'";
      }'
  fi


  if [ ! -f "006.stat_find_xdev_inum.out" -a ! -f "006.istat_find_xdev_inum.out" ]
  then
    linfo "No file named [006.(i)stat_find_xdev_inum.out]! Ignoring. Not creating inode-path.csv file..."
  else
    INODEPATHS_FILENAME=${MARKUP_PREFIX}inode-path.${MARKUP_SUFFIX}
    printf "#inode,path\n" >${INODEPATHS_FILENAME}

    grep -q -s -m 1 "^Found .* for inode .*$" 006.*_find_xdev_inum.out
    if [ $? -eq 0 ]
    then
      cat 006.*_find_xdev_inum.out | grep -v "^#" | awk '
        /^Found .* for inode .*$/
        {
          path=$0;
          sub (/^Found /, "", path);
          sub (/ for inode .*$/, "", path);

          inum=$0;
          sub (/^Found .* for inode /, "", inum);

          printf ("%s,%s\n", inum, path) >> "'${INODEPATHS_FILENAME}'";
        }' >/dev/null
    elif [ $? -eq 1 ]
    then
      cat 006.*_find_xdev_inum.out | grep -e "^### *stat" -e "^Found " | awk '
        BEGIN {inumAreThere = 0; pathAreThere = 0;}
        /^Found /
        {
          if ((0 == pathAreThere) && (1 == inumAreThere))
          {
            path = $0;
            sub (/^Found /, "", path);
            sub (/ \(no stat\/istat on .*\)$/, "", path);
            pathAreThere = 1;
          }
        }
        /^### .*stat .find "..*" .*.$/
        {
          if ((0 == inumAreThere) && (0 == pathAreThere))
          {
            inum = $0;
            sub (/^### .* -inum /, "", inum);
            sub (/\).*$/, "", inum);
            inumAreThere = 1;
          }
        }
        {
          if ((1 == inumAreThere) && (1 == pathAreThere))
          {
            printf ("%s,%s\n", inum, path) >> "'${INODEPATHS_FILENAME}'";
            inum = "";
            path = "";
            inumAreThere = 0;
            pathAreThere = 0;
          }
        }' >/dev/null
    fi
  fi

  printf "#This file is empty - look into proc-inode (and inode-path) CSV files instead\n"

  fn_print_output_filenames ${PROCINODES_FILENAME} ${INODEPATHS_FILENAME}
}
