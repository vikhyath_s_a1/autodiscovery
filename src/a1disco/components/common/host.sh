#!/bin/bash

fn_HOST_cql ()
{
  printf "node (:HOST {\n" 
  printf "  %*s : \"%s\",\n" ${PROPNAMEWIDTH} "d_host_id"        "${HOST_ID}"
  printf "  %*s : \"%s\",\n" ${PROPNAMEWIDTH} "d_uname"          "${UNAME}"
  printf "  %*s : \"%s\",\n" ${PROPNAMEWIDTH} "d_component_type" "HOST_${UNAME}"
  printf "  %*s : \"%s\",\n" ${PROPNAMEWIDTH} "d_hostname"       "`cat 001.hostname.out`"
  printf "  %*s : \"%s\",\n" ${PROPNAMEWIDTH} "d_uname_a"        "`cat 001.uname_a.out`"
  for file in 001.*.out
  do
    DNAME=${file}
    DNAME=${DNAME%.out}
    DNAME=${DNAME//001.}
    [ "${DNAME}" = "hostname" ] && continue
    [ "${DNAME}" = "uname_a" ] && continue
    FUNCTION_NAME="fn_HOST_${DNAME}_${MARKUP_SUFFIX}"
    declare -fF "${FUNCTION_NAME}" >/dev/null
    if [ $? -eq 0 ]
    then
      ${FUNCTION_NAME} ${file}
    else
      lerror "No function named 'fn_HOST_${DNAME}'! Cannot process file [${file}]"
    fi
  done
  printf "  %*s : \"%s\"\n" ${PROPNAMEWIDTH} "d_observation_ts" "${OBSRVTS}"
  printf "})\n"

  fn_print_output_filenames
}

fn_HOST_csv ()
{
  printf "%s,%s\n" "d_host_id"        "${HOST_ID}"
  printf "%s,%s\n" "d_uname"          "${UNAME}"
  printf "%s,%s\n" "d_component_type" "HOST_${UNAME}"
  printf "%s,%s\n" "d_hostname"       "`cat 001.hostname.out`"
  printf "%s,%s\n" "d_uname_a"        "`cat 001.uname_a.out`"
  for file in 001.*.out
  do
    DNAME=${file}
    DNAME=${DNAME%.out}
    DNAME=${DNAME//001.}
    [ "${DNAME}" = "hostname" ] && continue
    [ "${DNAME}" = "uname_a" ] && continue
    FUNCTION_NAME="fn_HOST_${DNAME}_${MARKUP_SUFFIX}"
    declare -fF "${FUNCTION_NAME}" >/dev/null
    if [ $? -eq 0 ]
    then
      ${FUNCTION_NAME} ${file}
    else
      lerror "No function named 'fn_HOST_${DNAME}_'! Cannot process file [${file}]"
    fi
  done
  printf "%s,%s\n" "d_observation_ts" "${OBSRVTS}"

  fn_print_output_filenames
}
