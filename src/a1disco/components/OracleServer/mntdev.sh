#!/bin/bash


fn_MNTDEV_csv()
{
  if [ ! -f "003.mount.out" ]
  then
    lerror "No file named [003.mount.out]! Cannot create (:MNTDEV)s"
    return 1
  fi

  printf "#device,mount-point,file-system,fs-flags\n"

  cat 003.mount.out | awk '
     {
       devFsPath = $1;
       mountPoint = $3;
       fileSystem = $5;
       fsFlags = $6;
       printf ("%s,%s,%s,%s\n",
         devFsPath, mountPoint, fileSystem, fsFlags);
     }'

  fn_print_output_filenames
}

