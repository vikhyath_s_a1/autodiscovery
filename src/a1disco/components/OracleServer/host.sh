#!/bin/bash

fn_HOST_lsb_release_drci_csv()
{
  cat 001.lsb_release_drci.out | awk -F ':' '
    {
      name = $1
      gsub (/  *$/, "", name);
      gsub (/^  */, "", name);
      gsub (/  */, "_", name);
      gsub (/^/, "d_'${UNAME}'_lsb_release_drci_", name);
  
      value = $2;
      gsub (/  *$/, "", value);
      gsub (/^  */, "", value);
  
      printf ("%s,%s\n", name, value);
    }'
}

fn_HOST_lscpu_csv()
{
  cat 001.lscpu.out | awk -F ':' '
    {
      name = $1
      gsub (/  *$/, "", name);
      gsub (/^  */, "", name);
      gsub (/  */, "_", name);
      gsub (/^/, "d_'${UNAME}'_lscpu_", name);
  
      value = $2;
      gsub (/  *$/, "", value);
      gsub (/^  */, "", value);
  
      printf ("%s,%s\n", name, value);
    }'
}

fn_HOST_proc_meminfo_csv()
{
  cat 001.proc_meminfo.out | grep -e "Mem" -e "Swap" | grep "Total" | awk -F ':' '
    {
      name = $1
      gsub (/  *$/, "", name);
      gsub (/^  */, "", name);
      gsub (/  */, "_", name);
      gsub (/^/, "d_'${UNAME}'_proc_meminfo_", name);
  
      value = $2;
      gsub (/  *$/, "", value);
      gsub (/^  */, "", value);
  
      printf ("%s,%s\n", name, value);
    }'
}

