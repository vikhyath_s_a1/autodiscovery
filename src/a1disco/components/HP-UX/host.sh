#!/bin/bash

fn_HOST_ioscan_kfnC_processor_csv()
{
  printf "%s,%s\n" "d_${UNAME}_oslevel" "`cat 001.oslevel.out`"
}

fn_HOST_machinfo_v_csv()
{
  cat 001.lparstat_i.out | awk -F ':' '
    /: \-$/ {next;}
    {
      captureParam = 0;
      if ($1 ~ /Virtual CPUs/)
        captureParam = 1;
      if ($1 ~ /Physical CPUs/)
        captureParam = 1;
      if ($1 ~ /Memory/)
        captureParam = 1;
      if (captureParam) {
        name = $1;
        gsub (/  *$/, "", name);
        gsub (/^  */, "", name);
        gsub (/  */, "_", name);
        gsub (/^/, "d_'${UNAME}'_lparstat_i_", name);
  
        value = $2;
        gsub (/  *$/, "", value);
        gsub (/^  */, "", value);
  
        printf ("%s,%s\n", name, value);
      }
    }'
  #printf "  %38s : \"%s\",\n" "d_${UNAME}_lparstat_i" "`cat $1`"
}
