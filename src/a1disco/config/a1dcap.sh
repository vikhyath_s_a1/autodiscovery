#!/bin/sh

# Logging settings
LOGSETTING_VERBOSITY=4
LOGSETTING_LOGTS=0
LOGSETTING_LOGPID=0
export LOGSETTING_VERBOSITY
export LOGSETTING_LOGTS
export LOGSETTING_LOGPID

# Daemon loop sleep interval (in seconds)
RUN_INTERVAL=30
export RUN_INTERVAL

# Directory in which to store the raw captures
RAWCAPDIR=`pwd`/disco_${YYYYMMDDHHMISS}
export RAWCAPDIR

linfo "============== The configuration parameters that were loaded ... =============="
linfo "           Verbosity of Logging : [${LOGSETTING_VERBOSITY}]"
linfo "              Timestamp Logging : [${LOGSETTING_LOGTS}]"
linfo "             Process-ID Logging : [${LOGSETTING_LOGPID}]"
linfo "  Capture interval (in seconds) : [${RUN_INTERVAL}]"
linfo "  ---------------------------------------------------------------------------"
linfo "          Raw capture directory : [${RAWCAPDIR}]"
linfo "==============================================================================="

