#!/bin/sh

# Width used to align property-names while printing
PROPNAMEWIDTH=42
export PROPNAMEWIDTH

# Prefix & Suffix for master entity markup files
MARKUP_PREFIX=ntts-
MARKUP_SUFFIX=csv
export MARKUP_PREFIX
export MARKUP_SUFFIX

# Logging settings
LOGSETTING_VERBOSITY=5
LOGSETTING_LOGTS=0
LOGSETTING_LOGPID=0
export LOGSETTING_VERBOSITY
export LOGSETTING_LOGTS
export LOGSETTING_LOGPID

linfo "============== The configuration parameters that were loaded ... =============="
linfo "           Verbosity of Logging : [${LOGSETTING_VERBOSITY}]"
linfo "              Timestamp Logging : [${LOGSETTING_LOGTS}]"
linfo "             Process-ID Logging : [${LOGSETTING_LOGPID}]"
#linfo "  ---------------------------------------------------------------------------"
linfo "==============================================================================="

