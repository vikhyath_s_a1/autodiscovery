#!/usr/bin/env bash

PROGNAME=$0
if [ "${PROGNAME}" = "-bash" ]
then
  PROGNAME=${BASH_SOURCE}
else
  echo
  echo "Please source this script: '. ${PROGNAME}'"
  echo
  exit 1
fi

#declare -r SCRIPT_NAME=$(readlink -f ${BASH_SOURCE[0]})
echo PROGNAME=${PROGNAME}
PROGBASENAME=`basename "${PROGNAME}"`
BASEDIR=`dirname "${PROGNAME}"`
LIBDIR=${BASEDIR}/lib
CFGDIR=${BASEDIR}/config
WRKDIR=${BASEDIR}/work
CMPDIR=${BASEDIR}/components

export BASEDIR
export LIBDIR
export CFGDIR
export WRKDIR

YYYYMMDDHHMISS=`date +"%Y%m%d%H%M%S"`
export YYYYMMDDHHMISS

. ${LIBDIR}/common/lib.sh
. ${LIBDIR}/common/logging.sh

fnuname
linfo "Loading common library functions..."
for file in ${LIBDIR}/common/*.sh
do
  linfo "  sourcing [${file}]..."
  . ${file}
done


