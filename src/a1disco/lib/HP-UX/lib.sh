fnaddprs()
{
  PREREQS="${PREREQS} machinfo ioscan nwmgr"
  export PREREQS
}

fnhost()
{
  linfo "  ${UNAME}: (ioscan -kfnC processor, machinfo -v)"

  ioscan -kfnC processor 1>${FNPREFIX}.ioscan_kfnC_processor.out 2>${FNPREFIX}.ioscan_kfnC_processor.err
  cat ${FNPREFIX}.ioscan_kfnC_processor.out

  machinfo -v 1>${FNPREFIX}.machinfo_v.out 2>${FNPREFIX}.machinfo_v.err
  cat ${FNPREFIX}.machinfo_v.out
}

fnnet()
{
  linfo "  ${UNAME}: (nwmgr -v -S iether, ifconfig <ifname>)"

  NWMGR_S_IETHER_OUTFILE=${FNPREFIX}.nwmgr_v_S_iether
  nwmgr -S iether 1>${NWMGR_S_IETHER_OUTFILE}.out 2>${NWMGR_S_IETHER_OUTFILE}.err

  tail +5 ${NWMGR_S_IETHER_OUTFILE}.out | awk '{print $1;}' | sort | uniq | while read ifn
  do
    linfo "  Retrieving configuration of interface ${ifn} ..."
    linfo "    (ifconfig ${ifn}) ..."
    ifconfig ${ifn} 1>${FNPREFIX}.ifconfig_${ifn}.out 2>${FNPREFIX}.ifconfig_${ifn}.err
  done
  
  LOWESTMACADDR=`tail +5 ${NWMGR_S_IETHER_OUTFILE}.out | awk '
  {
    toPrint = toupper($3);
    doPrint = 0;

    if (toPrint ~ /^(0X([0-9A-Fa-f]{2}){6})$/) {
      toPrint = substr (toPrint, 3);
      doPrint = 1;
    }

    if (toPrint ~ /^([0-9A-F]{1,2}[\.:]){5}([0-9A-F]{1,2})$/)
      doPrint = 1;
  
    if (1 == doPrint) {
      gsub (/\./, ":", toPrint);
      gsub (/:/, "_", toPrint);
      print toPrint;
    }
  }
  '|sort|head -1`
  export LOWESTMACADDR

}

fnpath()
{
  linfo "  ${UNAME}: (stat, find <name> -xdev -inum <inode>)"

  NUM_UNIQ_VREG_OPENFILES=`cat ${LSOF_P_FILE}.out | awk '($5 ~ /^REG$/) {
    printf ("%s %s ", $6, $8);
    $1=$2=$3=$4=$5=$6=$7=$8="";
    printf ("%s\n", $0);
  }' | sort -n | uniq | wc -l`
  linfo "  Number of unique REG open files (from lsof -lnP -bw -p): ${NUM_UNIQ_VREG_OPENFILES}"
  linfo "  No stat/istat on ${UNAME}. Only finding path of file."

  STAT_FIND_XDEV_INUM_FILE=${FNPREFIX}.stat_find_xdev_inum
  cat ${LSOF_P_FILE}.out | awk '($5 ~ /^REG$/) {
    printf ("%s %s ", $6, $8);
    $1=$2=$3=$4=$5=$6=$7=$8="";
    printf ("%s\n", $0);
  }' | sort -n | uniq | while read DEVICE NODE NAME
  do
    NAME=`echo ${NAME} | sed -e 's/ -- .*$//g' -e 's/ *(.*)$//g'`
    [ ${LOGSETTING_VERBOSITY} -lt ${LEVEL_DEBUG} ] && printf "."
    ldebug "    From lsof...-p: ${DEVICE} ${NODE} \"${NAME}\""
    echo "### stat (find \"${NAME}\" -xdev -inum ${NODE})" >> ${STAT_FIND_XDEV_INUM_FILE}.out
    echo "### stat (find \"${NAME}\" -xdev -inum ${NODE})" >> ${STAT_FIND_XDEV_INUM_FILE}.err
    FILEPATH=`find "${NAME}" -xdev -inum ${NODE} 2>&1`
    if [ $? -ne 0 ]
    then
      [ ${LOGSETTING_VERBOSITY} -lt ${LEVEL_DEBUG} ] && printf "\n"
      lerror "    (find \"${NAME}\" -xdev -inum ${NODE}) failed: [${FILEPATH}]"
      echo "(find \"${NAME}\" -xdev -inum ${NODE}) failed: [${FILEPATH}]" >>${STAT_FIND_XDEV_INUM_FILE}.err
    else
      ldebug "    Found ${FILEPATH}"
      echo "Found ${FILEPATH} (no stat/istat on ${UNAME})" >> ${STAT_FIND_XDEV_INUM_FILE}.out
      #stat ${FILEPATH} 1>>${STAT_FIND_XDEV_INUM_FILE}.out 2>>${STAT_FIND_XDEV_INUM_FILE}.err
    fi
  done
  [ ${LOGSETTING_VERBOSITY} -lt ${LEVEL_DEBUG} ] && printf "\n"
}

