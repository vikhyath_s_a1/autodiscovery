LOGSETTING_VERBOSITY=4
LOGSETTING_LOGTS=0
LOGSETTING_LOGPID=0

LEVEL_SILENT=0
LEVEL_CRITICAL=1
LEVEL_ERROR=2
LEVEL_WARN=3
LEVEL_INFO=4
LEVEL_DEBUG=5

lprintf_intern()
{
  LOGLEVEL_NUM=$1
  LOGLEVEL_STR=$2
  FORMAT_STR=$3
  shift
  shift
  shift
  if [ ${LOGLEVEL_NUM} -le ${LOGSETTING_VERBOSITY} ]
  then
    if [ ${LOGSETTING_LOGTS} -eq 1 ]
    then
      TIMESTAMP=`date +'%Y-%m-%d %H:%M:%S'`
      printf "[%s] " "${TIMESTAMP}"
    fi
    if [ ${LOGSETTING_LOGPID} -eq 1 ]
    then
      PROCESSID=$$
      printf "[%d] " ${PROCESSID}
    fi
    printf "[%-5s] " "${LOGLEVEL_STR}"
    printf "${FORMAT_STR}" $*
    echo
  fi
  return 0
}

lsilent()
{
  FORMAT_STR=$1
  shift
  lprintf_intern ${LEVEL_SILENT}   "SILENT"   "${FORMAT_STR}" $*
}

lcritical()
{
  FORMAT_STR=$1
  shift
  lprintf_intern ${LEVEL_CRITICAL} "CRITICAL" "${FORMAT_STR}" $*
}
lerror()
{
  FORMAT_STR=$1
  shift
  lprintf_intern ${LEVEL_ERROR}    "ERROR"    "${FORMAT_STR}" $*
}
lwarn()
{
  FORMAT_STR=$1
  shift
  lprintf_intern ${LEVEL_WARN}  "WARN"  "${FORMAT_STR}" $*
}
linfo()
{
  FORMAT_STR=$1
  shift
  lprintf_intern ${LEVEL_INFO}     "INFO"     "${FORMAT_STR}" $*
}
ldebug()
{
  FORMAT_STR=$1
  shift
  lprintf_intern ${LEVEL_DEBUG}    "DEBUG"    "${FORMAT_STR}" $*
}
