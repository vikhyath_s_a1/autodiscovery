#!/bin/bash

fntakein_usage_intern()
{
  lerror ""
  lerror "  Usage : ${FUNCNAME[0]//_usage_intern/} <prompt> <evar> [-p]"
  lerror "    <prompt> : prompt to display to elicit input"
  lerror "      <evar> : environment variable to set with input"
  lerror "          -p : Optional: treat input like password"
  lerror "               (i.e. input should be echoed to screen)"
  lerror ""
}
fntakein()
{
  if [ $# -lt 2 ]
  then
    ${FUNCNAME[0]}_usage_intern
    return 1
  fi
  __PPT=$1; shift
  __VAR=$1; shift
  __PASS=
  __READ_ARG=
  [ $# -ne 0 ] && __PASS=$1; shift

  printf "%s: " "${__PPT}" >&2
  [ "-p" = "${__PASS}" ] && __READ_ARG="-s"
  read ${__READ_ARG} "${__VAR}"
  [ "-s" = "${__READ_ARG}" ] && printf "\n"
  __PPT=
  __VAR=
  __PASS=
  __READ_ARG=
  return 0
}

fnchoose_usage_intern()
{
  lerror ""
  lerror "  Usage : ${FUNCNAME[0]//_usage_intern/} <prompt> <evar> <opt1> <opt1-desc> [<opt2> <opt2-desc> ...]"
  lerror "      <prompt> : prompt to display to elicit input"
  lerror "        <evar> : environment variable to set with input"
  lerror "        <optN> : N'th input option"
  lerror "   <optN-desc> : Brief description of N'th input option"
  lerror "  where 'N' is 1 or more..."
  lerror ""
}

fnchoose()
{
  _PPT=$1; shift
  _VAR=$1; shift
  _VAL=
  _OPTS=":"
  _DSCS=":"
  while [ "" = "${_VAL}" ]
  do
    if [ ":" = "${_OPTS}" ]
    then
      linfo "${_PPT} -"
      while [ $# -ne 0 ]
      do
        [ $# -ne 0 ] && _OPT="$1"; shift
        [ $# -ne 0 ] && _DSC="$1"; shift
        if [ -z "${_OPT}" -o -z "${_DSC}" ]
        then
          ${FUNCNAME[0]}_usage_intern
          return 1
        fi
        linfo "  %6s ${_DSC}" "(${_OPT})"
        _OPTS="${_OPTS}${_OPT}:"
        _DSCS="${_DSCS}${_DSC}:"
      done
    fi
    if [ ":" = "${_OPTS}" ]
    then
      ${FUNCNAME[0]}_usage_intern
      return 1
    fi

    fntakein "${_PPT}" "${_VAR}"
    _VAL=`eval echo \\${${_VAR}}`
    echo "${_OPTS}" | grep -q ":${_VAL}:"
    if [ $? -eq 0 ]
    then
      linfo "Thank you for choosing option (${_VAL})"
      return 0
    fi
    linfo "Invalid input (${_VAL}). Please try again..."
    _VAL=
  done
  return 1;
}

