fnuname()
{
  UNAME=`uname`
  linfo "UNAME is ${UNAME}"
  if [ "${UNAME}" = "Linux" ]
  then
    linfo "  Checking Linux flavour using 'lsb_release'"
    LSB_RELEASE=`which lsb_release 2>&1`
    if [ $? -ne 0 ]
    then
      lwarn "  Location of [lsb_release] failed! [${LSB_RELEASE}]"
      lwarn "  Continuing with UNAME ${UNAME} ..."
    else
      if [ -f "${LSB_RELEASE}" ]
      then
        linfo "  Located [lsb_release] @ [${LSB_RELEASE}]"
        UNAME=`lsb_release -i | awk -F ':' '
          {
            gsub(/^[ \t]*/, "", $2);
            gsub(/[ \t]*$/, "", $2);
            print $2;
          }'`
        if [ -d ${LIBDIR}/${UNAME} ]
        then
          linfo "  lsb_release gave UNAME ${UNAME}, and lib folder present ..."
        else
          lwarn "  lsb_release gave UNAME ${UNAME}, but lib folder missing ..."
          UNAME=Linux
          lwarn "  Continuing with UNAME ${UNAME} ..."
        fi
      else
        lwarn "  Location of [lsb_release] failed! (${LSB_RELEASE})"
        lwarn "  Continuing with UNAME ${UNAME} ..."
      fi
    fi
  fi
  export UNAME
}

fnaddprs()
{
  linfo "Dummy function to be overriden to add pre-requisite commands"
}

fnprchk()
{
  PRNF=0
  dep=
  depp=
  unmetdeps=
  metdeps=
  linfo "Checking for pre-requisites ($*)..."
  for dep in "$@"
  do
    depp=`which ${dep} 2>&1`
    if [ $? -ne 0 ]
    then
      PRNF=1
      lerror "  Location of [%s] failed! (${depp})" "${dep}"
      unmetdeps="${unmetdeps}${dep},"
    else
      if [ -f "${depp}" ]
      then
        linfo "  Located [%12s] @ [${depp}]" "${dep}"
        metdeps="${metdeps}${dep},"
      else
        PRNF=1
        lerror "  Location of [%s] failed! (${depp})" "${dep}"
        unmetdeps="${unmetdeps}${dep},"
      fi
    fi
  done
  if [ ${PRNF} -eq 1 ]
  then
    unmetdeps=${unmetdeps%,}
    metdeps=${metdeps%,}
    linfo ""
    linfo "Following pre-requisites have been met :"
    linfo "  [${metdeps}]"
    linfo ""
    lerror "===================================================================="
    lerror "Following pre-requisites have NOT been met :"
    lerror "  [${unmetdeps}]"
    lerror ""
    lerror "Kindly ensure all pre-requisites are met before retrying this script"
    lerror "===================================================================="
    fnfinishup
  fi
  linfo ""
  return 0
}

fnpfx()
{

  if [ "${FNCOUNTER}" = "" ]
  then
    FNCOUNTER=0
  fi
  FNCOUNTER=`expr $FNCOUNTER + 1`
  if [ ! -z "${RAWCAPDIR}" ]
  then
    FNPREFIX="${RAWCAPDIR}/`printf "%03d" $FNCOUNTER`"
  elif [ ! -z "$1" ]
  then
    FNPREFIX="$1/`printf "%03d" $FNCOUNTER`"
  else
    FNPREFIX=`printf "%03d" $FNCOUNTER`
  fi
  export FNCOUNTER
  export FNPREFIX
}

fnrm0()
{
  linfo "Cleaning up empty files in $1..."
  ls -l $1 | awk '($5 ~ /^00*$/) {printf ("echo \"[$$]:   Removing empty file [%s]...\"\nrm -f '$1'/%s\n", $9, $9);}' | $SHELL
  for file in $1/*
  do
    numLines=`grep -v "^#" ${file} | wc -l`
    if [ ${numLines} -eq 0 ]
    then
      printf "[$$]:   Removing empty file [%s/%s]...\n" "`basename "$1"`" "`basename "${file}"`"
      rm -f ${file}
    fi
  done
}

fntargzrm()
{
  linfo "TARring and GZipping $1, and removing the directory..."
  tar -cvf $1.tar `basename $1` && gzip -v $1.tar && rm -rf $1
}

fnsiginparent()
{
  linfo "Trapped SIGINT/SIGTERM - killing child process [${CPID}]..."
  kill ${CPID}
}

fnsiginchild()
{
  linfo "Caught SIGINT/SIGTERM. Finishing up..."
  fnfinishup
}

fnstartup ()
{
  DISP_START_TS=`date "+%Y-%m-%d %H:%M:%S"`
  MODE=1
  while [ ${MODE} -eq 1 ]
  do
    linfo "Loading configuration from [${CFGDIR}/${PROGBASENAME}]..."
    . ${CFGDIR}/${PROGBASENAME}
    linfo ""

    fnmain $*

    if [ ${RUNASDAEMON} -eq 1 ]
    then
      ldebug "  Sleeping for [${CAPTURE_INTERVAL}] seconds"
      linfo ""
      sleep ${CAPTURE_INTERVAL}
    else
      MODE=0
    fi
  done
}

fnfinishup()
{
  DISP_END_TS=`date "+%m-%d-%Y_%H:%M:%S"`

  linfo ""
  linfo "Done DonA Done Done"
  linfo "==================="
  if [ ! "${DISP_START_TS}" = "" ]
  then
    linfo "  + Ran from [${DISP_START_TS}] to [${DISP_END_TS}]..."
  fi
  linfo "  + Terminal output has been saved in %-42s" "${SELFLOGFILE}"
  linfo "                                      ------------------------------------------"
  linfo "=================================================================================="
  linfo ""

  rm -f ${RUNNINGFILE}

  exit 0
}

fncheckrunning()
{
  if [ -f ${RUNNINGFILE} ]
  then
    linfo "#########################################################################"
    linfo "This program is already running, PID [%s]" "`cat ${RUNNINGFILE}`"
    linfo "Exitting..."
    linfo ""
    linfo "(If the file is from a previous run's improper termination,"
    linfo " please remove the file, and retry running this script.)"
    linfo "#########################################################################"
    exit 1
  else
    echo $$ >${RUNNINGFILE}
    return 0
  fi
}

fnls1line()
{
  ls -1 $* | tr -s '\n' ' ' | sed -e 's/^  *//' -e 's/  *$//'
}

fnmntdev()
{
  linfo "  (mount, df -k)"

  mount 1>${FNPREFIX}.mount.out 2>${FNPREFIX}.mount.err
  df -k 1>${FNPREFIX}.df_k.out 2>${FNPREFIX}.df_k.err
}

fnprocaddr()
{
  linfo "  (lsof -lnP -bw -i,"
  linfo "   ps -e -o pid,ppid,user,group,ruser,rgroup,etime,time,comm,args)"

  LSOF_I_FILE=${FNPREFIX}.lsof_i
  lsof -lnP -bw -i 1>${LSOF_I_FILE}.out 2>${LSOF_I_FILE}.err
  linfo "  Found `wc -l ${LSOF_I_FILE}.out|awk '{print $1;}'` active TCP addresses..."

  #case ${UNAME} in
  #  "HP-UX")
  #    # Required for 'ps -o <format>' to work on HP-UX
      UNIX_STD=2003
      export UNIX_STD
  #    ;;
  #esac

  PS_E_O_FILE=${FNPREFIX}.ps_e_o
  cat ${LSOF_I_FILE}.out | awk ' $8 ~ /^TCP$/ { print $2; }' | sort -n | uniq | while read NPID
    do
      linfo "  Listing TCP-active process ID ${NPID} (ps -e -o...) ..."
      echo "### ps -e -o pid,ppid,user,group,ruser,rgroup,etime,time,comm,args... for ${NPID}" >>${PS_E_O_FILE}.out
      echo "### ps -e -o pid,ppid,user,group,ruser,rgroup,etime,time,comm,args... for ${NPID}" >>${PS_E_O_FILE}.err
      ps -e -o pid,ppid,user,group,ruser,rgroup,etime,time,comm,args|awk '/^ *'${NPID}' /' 1>>${PS_E_O_FILE}.out 2>>${PS_E_O_FILE}.err
    done
}

fnprocpath()
{
  linfo "  (lsof -lnP -bw -p <pid>)"

  LSOF_P_FILE=${FNPREFIX}.lsof_p
  NUMTCPACTIVEPROCS=`cat ${PS_E_O_FILE}.out | awk '/^### / {next;} {printf ("%s\n%s\n", $1, $2);}' | sort -n | uniq | wc -l`
  linfo "  Found ${NUMTCPACTIVEPROCS} TCP-active processes ..."
  #cat ${PS_E_O_FILE}.out | awk '/^### / {next;} { printf ("%s\n", $1);}' | sort -n | uniq | while read NPID
  cat ${PS_E_O_FILE}.out | awk '/^### / {next;} { printf ("%s\n%s\n", $1, $2);}' | sort -n | uniq | while read NPID
    do
      linfo "  Listing open files in TCP-active (parent) process ID ${NPID} (lsof -lnP -bw -p...) ..."
      echo "### lsof -lnP -bw -p ${NPID}" >>${LSOF_P_FILE}.out
      echo "### lsof -lnP -bw -p ${NPID}" >>${LSOF_P_FILE}.err
      lsof -lnP -bw -p ${NPID} 1>>${LSOF_P_FILE}.out 2>>${LSOF_P_FILE}.err
    done
}

fnEnsureAbsolute()
{
  PathName=$1
  PathValue=$2

  if [[ ${PathValue} == /* ]]
  then
    linfo "# Absolute path supplied: ${PathName}=[${PathValue}] Using it as is."
    PathValue="${PathValue}"
    export $PathName="$PathValue"
  else
    RelPathValue=${PathValue}
    PathValue="`pwd`/${PathValue}"
    export $PathName="$PathValue"
    #echo "# Relative path supplied: ${PathName}=[${RelPathValue}]"
    linfo "#   Changed to absolute path [${PathValue}]"
  fi
}

fnEnsureDirectory()
{
  PathName=$1
  PathValue=$2
  linfo "# Ensuring directory ${PathName}=${PathValue} exists"
  mkdir -p $PathValue
}

