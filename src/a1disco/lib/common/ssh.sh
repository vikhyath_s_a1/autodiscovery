#!/bin/bash

fnsshplls_usage_intern()
{
    lerror ""
    lerror "  Usage : ${FUNCNAME[0]//_usage_intern/} <id_rsa> <host> <user> [<port>]"
    lerror ""
}

fnsshplls()
{
  if [ $# -lt 3 ]
  then
    ${FUNCNAME[0]}_usage_intern $0
    return 1
  fi
  _IDRSA=$1; shift
  _HOST=$1; shift
  _USER=$1; shift
  [ $# -ne 0 ] && _PORT=$1; shift
  [ -z "${_PORT}" ] && _PORT=22

  _CMD="ssh-copy-id -i ${_IDRSA}"
  [ -n "${_PORT}" ] && _CMD="${_CMD} -p ${_PORT}"
  _CMD="${_CMD} -o User=${_USER} ${_HOST}"

  linfo "Copying ${_IDRSA} to ${_USER} @ ${_HOST} ..."
  ldebug "Command for copying ${_IDRSA} to ${_USER}@${_HOST}..."
  ldebug "  Executing command : [${_CMD}] ..."
  ldebug "================================================================================"
  ${_CMD} <<EOF
echo "It works!"
EOF
  _STATUS=$?
  ldebug "================================================================================"
  if [ ${_STATUS} -ne 0 ]
  then
    lerror "*** Failed to copy ${_IDRSA} to ${_USER} @ ${_HOST}"
    lerror "*** Did you get the password right?"
    return 2
  fi
  linfo "Successfully copied ${_IDRSA} to ${_USER} @ ${_HOST}"

  _CMD=${_CMD//ssh-copy-id /ssh }

  linfo "Attempting ssh using ${_IDRSA}, to check password-less sign-in ..."
  ldebug "Command for testing password-less sign-in to ${_USER}@${HOST}..."
  ldebug "  Executing command : [${_CMD}] ..."
  ldebug "================================================================================"
  ${_CMD} <<EOF
echo "Hello"
echo "pwd"
pwd
echo "ls -alrt"
ls -alrt
echo "cat ~/.ssh/authorized_keys"
cat ~/.ssh/authorized_keys
echo "Goodbye"
EOF
  _STATUS=$?
  ldebug "================================================================================"

  _HOST=
  _PORT=
  _USER=
  _PASS=
  _CMD=
  _STATUS=
}

fnsshsus_usage_intern()
{
    lerror ""
    lerror "  Usage : ${FUNCNAME[0]//_usage_intern/} <host> <root> <user> [<port>]"
    lerror "    <host>   - host on which to setup sudo-ready OS-user-id"
    lerror "    <root>   - either 'root', or an OS-user-id with sudo privilege"
    lerror "    <user>   - OS-user-id to setup"
    lerror "  [ <port> ] - Optional: ssh port number"
    lerror "               (defaulted to 22, if unsupplied)"
    lerror ""
}


fnsshsus()
{
  if [ $# -lt 3 ]
  then
    ${FUNCNAME[0]}_usage_intern
    return 1
  fi
  #
  # Check if user is already present on that host
  # IF USER ALREADY PRESENT
  #   Setup sudoers for the user
  # ELSE
  #   Attempt user creation, with hard-coded password
  #   Setup sudoers for the user
  #

  _HOST=$1; shift
  _ROOT=$1; shift
  _USER=$1; shift
  _SUDO="";
  [ $# -ne 0 ] && _PORT=$1; shift
  [ -z "${_PORT}" ] && _PORT=22; shift

  while [ 1 ]
  do
    linfo "Attempting sign-in as ${_ROOT} @ ${_HOST} : ${_PORT} ..."
    _CMD="ssh"
    [ -n "${_PORT}" ] && _CMD="${_CMD} -p ${_PORT}"
    _CMD="${_CMD} -o User=${_ROOT} ${_HOST}"
    ldebug "Command to attempt signin is [${_CMD}]..."
    ldebug "================================================================================"
    ${_CMD} <<EOF
echo "Successfully logged in using [${_CMD}]..."
exit
EOF
    _STATUS=$?
    ldebug "================================================================================"

    if [ ${_STATUS} -eq 0 ]
    then
      linfo "Successfully signed in as ${_ROOT} @ ${_HOST} : ${_PORT}"
      break
    else
      # Supplied user didn't work
      # Accept sudoer / root, user-id
      lerror "Could not sign-in as ${_ROOT} @ ${_HOST} : ${_PORT}."
      fnchoose "Enter your choice" "ruq" r "Retry (with new root password)" u "Retry (with new root user-id)" q "Quit/Exit/GiveUp"
      linfo "You entered [${ruq}]"
      case ${ruq} in
        r)
          linfo "Retrying with same user-id [${_ROOT}]..."
          ;;
        u)
          fntakein "Enter new root user-id" _ROOT
          linfo "Retrying with new root user-id [${_ROOT}]..."
          ;;
        q)
          linfo "Quitting..."
          return 2
          ;;
      esac
    fi
  done

  linfo "Attempting to create new OS user-id [${_USER}]..."
  _CMD="ssh -tt "
  [ -n "${_PORT}" ] && _CMD="${_CMD} -p ${_PORT}"
  _CMD="${_CMD} -o User=${_ROOT} ${_HOST}"
  if [ "${_ROOT}" = "root" ]
  then
    _SUDO=
  else
    _SUDO="sudo"
  fi
  ldebug "Command to attempt signin is [${_CMD}]..."
  ldebug "================================================================================"
  ${_CMD} <<EOF
PATH="${PATH}:/bin:/sbin:/usr/bin:/usr/sbin:/opt/csw/bin:/opt/csw/sbin"
echo ${PATH}
echo "${_SUDO} useradd -n ${_USER}"
${_SUDO} useradd -m ${_USER}
kn03v3n4g3l
EOF
  _STATUS=$?
  ldebug "================================================================================"

  _HOST=
  _USER=
  _SUDO=
  _ROOT=
  _PORT=
  _CMD=
  _STATUS=
  return 0
}

