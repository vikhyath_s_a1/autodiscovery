fnaddprs()
{
#  PREREQS="${PREREQS} stat lsb_release lscpu"
  PREREQS="${PREREQS} lsb_release lscpu"
  export PREREQS
}

fnhost()
{
  linfo "  ${UNAME}: (lsb_release -drci, lscpu, /proc/meminfo)"

  lsb_release -drci | tr -s '\t' ' ' 1>${FNPREFIX}.lsb_release_drci.out 2>${FNPREFIX}.lsb_release_drci.err
  lscpu 1>${FNPREFIX}.lscpu.out 2>${FNPREFIX}.lscpu.err
  cat /proc/meminfo 1>${FNPREFIX}.proc_meminfo.out 2>${FNPREFIX}.proc_meminfo.err
}

fnnet()
{
  linfo "  ${UNAME}: (netstat -i, ifconfig <ifname>)"

  netstat -i 1>${FNPREFIX}.netstat_i.out 2>${FNPREFIX}.netstat_i.err
  NETSTAT_I_OUTFILE=${FNPREFIX}.netstat_i.out

  tail -n +3 ${NETSTAT_I_OUTFILE} | awk '{print $1;}' | sort | uniq | while read ifn
  do
    linfo "  Retrieving configuration of interface ${ifn} ..."
    linfo "    (ifconfig ${ifn}) ..."
    ifconfig ${ifn} 1>${FNPREFIX}.ifconfig_${ifn}.out 2>${FNPREFIX}.ifconfig_${ifn}.err
  done

  grep -q -w ether ${FNPREFIX}.ifconfig_*.out
  TOK_ETHER=$?
  grep -q -w HWaddr ${FNPREFIX}.ifconfig_*.out
  TOK_HWADDR=$?

  if [ $TOK_ETHER -eq 0 ]
  then
    LOWESTMACADDR=`cat ${FNPREFIX}.ifconfig_*.out | awk ' $1 ~ /^ether$/ {print $2} ' | awk -F: '
    {printf ("%s_%s_%s_%s_%s_%s\n", $1, $2, $3, $4, $5, $6);}' | sort | head -1`
    export LOWESTMACADDR
    linfo "  Found lowest MAC address [${LOWESTMACADDR}] ..."
  elif [ $TOK_HWADDR -eq 0 ]
  then
    LOWESTMACADDR=`grep -w HWaddr ${FNPREFIX}.ifconfig_*.out | sed -e 's/^.* HWaddr \([^ ]*\) *$/\1/' | awk -F: '
    {printf ("%s_%s_%s_%s_%s_%s\n", $1, $2, $3, $4, $5, $6);}' | sort | head -1`
    export LOWESTMACADDR
    linfo "  Found lowest MAC address [${LOWESTMACADDR}] ..."
  else
    LOWESTMACADDR="NO_MA_CA_DD_RF_ND"
    export LOWESTMACADDR
    lwarn "  Did not find lowest MAC address. Using [${LOWESTMACADDR}] ..."
  fi
}

fnpath()
{
  linfo "  ${UNAME}: (stat, find <name> -xdev -inum <inode>)"

  NUM_UNIQ_VREG_OPENFILES=`cat ${LSOF_P_FILE}.out | awk '($5 ~ /^REG$/) {
    printf ("%s ", $6);
    if ($4 ~ /^DEL$/) {
      printf ("%s ", $7);
      $1=$2=$3=$4=$5=$6=$7="";
    } else {
      printf ("%s ", $8);
      $1=$2=$3=$4=$5=$6=$7=$8="";
    }
    printf ("%s\n", $0);
  }' | sort -n | uniq | wc -l`
  linfo "  Number of unique REG open files (from lsof -lnP -bw -p): ${NUM_UNIQ_VREG_OPENFILES}"

  STAT_FIND_XDEV_INUM_FILE=${FNPREFIX}.stat_find_xdev_inum
  cat ${LSOF_P_FILE}.out | awk '($5 ~ /^REG$/) {
    printf ("%s ", $6);
    if ($4 ~ /^DEL$/) {
      printf ("%s ", $7);
      $1=$2=$3=$4=$5=$6=$7="";
    } else {
      printf ("%s ", $8);
      $1=$2=$3=$4=$5=$6=$7=$8="";
    }
    printf ("%s\n", $0);
  }' | sort -n | uniq | while read DEVICE NODE NAME
  do
    [ ${LOGSETTING_VERBOSITY} -lt ${LEVEL_DEBUG} ] && printf "."
    ldebug "    From lsof...-p: ${DEVICE} ${NODE} \"${NAME}\""
    COMMANDSTR="find / -xdev -inum ${NODE}"
    FILEPATH=`${COMMANDSTR} 2>&1`
    if [ $? -ne 0 ]
    then
      [ ${LOGSETTING_VERBOSITY} -lt ${LEVEL_DEBUG} ] && printf "\n"
      lerror "    (${COMMANDSTR}) for (${NAME}) failed: [${FILEPATH}]"
      echo "### stat (${COMMANDSTR}) for (${NAME})" >> ${STAT_FIND_XDEV_INUM_FILE}.err
      echo "(${COMMANDSTR}) failed: [${FILEPATH}]" >>${STAT_FIND_XDEV_INUM_FILE}.err
    elif [ "${FILEPATH}" = "" ]
    then
      [ ${LOGSETTING_VERBOSITY} -lt ${LEVEL_DEBUG} ] && printf "\n"
      lerror "    (${COMMANDSTR}) for (${NAME}): empty FILEPATH: [${FILEPATH}]"
      echo "### stat (${COMMANDSTR}) for (${NAME})" >> ${STAT_FIND_XDEV_INUM_FILE}.err
      echo "(${COMMANDSTR}) returned empty FILEPATH: [${FILEPATH}]" >>${STAT_FIND_XDEV_INUM_FILE}.err
    else
      ldebug "    Found ${FILEPATH}"
      echo "### stat (${COMMANDSTR}) for (${NAME})" >> ${STAT_FIND_XDEV_INUM_FILE}.out
      echo "Found ${FILEPATH}"       >> ${STAT_FIND_XDEV_INUM_FILE}.out
      stat ${FILEPATH} 1>>${STAT_FIND_XDEV_INUM_FILE}.out 2>>${STAT_FIND_XDEV_INUM_FILE}.err
    fi
  done
  [ ${LOGSETTING_VERBOSITY} -lt ${LEVEL_DEBUG} ] && printf "\n"
}

