fnaddprs()
{
#  PREREQS="${PREREQS} stat isainfo psrinfo prtconf"
  PREREQS="${PREREQS} isainfo psrinfo prtconf"
  export PREREQS
}

fnhost()
{
  linfo "  ${UNAME}: (cat /etc/release, isainfo, psrinfo -pv, kstat cpu_info, prtconf -v)"

  cat /etc/release 1>${FNPREFIX}.cat_etc_release.out 2>${FNPREFIX}.cat_etc_release.err

  isainfo 1>${FNPREFIX}.isainfo.out 2>${FNPREFIX}.isainfo.err

  psrinfo -pv 1>${FNPREFIX}.psrinfo_pv.out 2>${FNPREFIX}.psrinfo_pv.err
  cat ${FNPREFIX}.psrinfo_pv.out

  kstat cpu_info 1>${FNPREFIX}.kstat_cpu_info.out 2>${FNPREFIX}.kstat_cpu_info.err
  cat ${FNPREFIX}.kstat_cpu_info.out | egrep "cpu_info |core_id" | awk '
    BEGIN { printf ("%4s\t%4s\n", "CPU", "core"); } 
    /^module:/ {printf ("%4s\t", $4);}
    /^\tcore_id/ {printf ("%4s\n", $2);}'

  prtconf -v 1>${FNPREFIX}.prtconf_v.out 2>${FNPREFIX}.prtconf_v.err
  cat ${FNPREFIX}.prtconf_v.out | awk '{ if ($0 ~ /^Memory size/) print $0; }'
}

fnnet()
{
  linfo "  ${UNAME}: (ifconfig -a)"

  IFCONFIG_A_FILE=${FNPREFIX}.ifconfig_a
  ifconfig -a 1>${IFCONFIG_A_FILE}.out 2>${IFCONFIG_A_FILE}.err
  LOWESTMACADDR=`grep "^	ether" ${IFCONFIG_A_FILE}.out | awk '
  {print $2}
  ' | awk -F: '
  {printf ("%s_%s_%s_%s_%s_%s\n", $1, $2, $3, $4, $5, $6);}
  ' | sort | head -1`
  export LOWESTMACADDR
}

fnmntdev()
{
  linfo "  ${UNAME}: (mount -p, df -k)"

  mount -p 1>${FNPREFIX}.mount_p.out 2>${FNPREFIX}.mount_p.err
  df -k 1>${FNPREFIX}.df_k.out 2>${FNPREFIX}.df_k.err
}

fnpath()
{
  linfo "  ${UNAME}: (stat, find <name> -xdev -inum <inode>)"

  NUM_UNIQ_VREG_OPENFILES=`cat ${LSOF_P_FILE}.out | awk '($5 ~ /^VREG$/) {
    printf ("%s %s ", $6, $8);
    $1=$2=$3=$4=$5=$6=$7=$8="";
    printf ("%s\n", $0);
  }' | sort -n | uniq | wc -l`
  linfo "  Number of unique VREG open files (from lsof -lnP -bw -p): ${NUM_UNIQ_VREG_OPENFILES}"

  STAT_FIND_XDEV_INUM_FILE=${FNPREFIX}.stat_find_xdev_inum
  cat ${LSOF_P_FILE}.out | awk '($5 ~ /^VREG$/) {
    printf ("%s %s ", $6, $8);
    $1=$2=$3=$4=$5=$6=$7=$8="";
    printf ("%s\n", $0);
  }' | sort -n | uniq | while read DEVICE NODE NAME
  do
    [ ${LOGSETTING_VERBOSITY} -lt ${LEVEL_DEBUG} ] && printf "."
    ldebug "    From lsof...-p: ${DEVICE} ${NODE} \"${NAME}\""
    echo "### stat (find \"${NAME}\" -xdev -inum ${NODE})" >> ${STAT_FIND_XDEV_INUM_FILE}.out
    echo "### stat (find \"${NAME}\" -xdev -inum ${NODE})" >> ${STAT_FIND_XDEV_INUM_FILE}.err
    FILEPATH=`find "${NAME}" -xdev -inum ${NODE} 2>&1`
    if [ $? -ne 0 ]
    then
      [ ${LOGSETTING_VERBOSITY} -lt ${LEVEL_DEBUG} ] && printf "\n"
      lerror "    (find \"${NAME}\" -xdev -inum ${NODE}) failed: [${FILEPATH}]"
      echo "(find \"${NAME}\" -xdev -inum ${NODE}) failed: [${FILEPATH}]" >>${STAT_FIND_XDEV_INUM_FILE}.err
    else
      ldebug "    Found ${FILEPATH}"
      echo "Found ${FILEPATH}" >> ${STAT_FIND_XDEV_INUM_FILE}.out
      stat ${FILEPATH} 1>>${STAT_FIND_XDEV_INUM_FILE}.out 2>>${STAT_FIND_XDEV_INUM_FILE}.err
    fi
  done
  [ ${LOGSETTING_VERBOSITY} -lt ${LEVEL_DEBUG} ] && printf "\n"
}

