fnaddprs()
{
#  PREREQS="${PREREQS} istat oslevel lparstat rmsock"
  PREREQS="${PREREQS} oslevel lparstat rmsock"
  export PREREQS
}

fnhost()
{
  linfo "  ${UNAME}: (oslevel, lparstat)"

  oslevel 1>${FNPREFIX}.oslevel.out 2>${FNPREFIX}.oslevel.err

  lparstat -i 1>${FNPREFIX}.lparstat_i.out 2>${FNPREFIX}.lparstat_i.err
  cat ${FNPREFIX}.lparstat_i.out | awk '{
  if ($0 ~ /^Maximum Virtual CPUs/)
    print $0;
  if ($0 ~ /^Maximum Memory/)
    print $0;
  }'
}

fnnet()
{
  linfo "  ${UNAME}: (netstat -i, ifconfig <ifname>)"

  netstat -i 1>${FNPREFIX}.netstat_i.out 2>${FNPREFIX}.netstat_i.err
  NETSTAT_I_OUTFILE=${FNPREFIX}.netstat_i.out
  
  tail +2 ${NETSTAT_I_OUTFILE} | awk '{print $1;}' | sort | uniq | while read ifn
  do
    linfo "  Retrieving configuration of interface ${ifn} ..."
    linfo "    (ifconfig ${ifn}) ..."
    ifconfig ${ifn} 1>${FNPREFIX}.ifconfig_${ifn}.out 2>${FNPREFIX}.ifconfig_${ifn}.err
  done
  
  LOWESTMACADDR=`tail +2 ${NETSTAT_I_OUTFILE} | awk '
  {
    toPrint = toupper($4);
    doPrint = 0;
    if (toPrint ~ /^([0-9A-F]{1,2}[\.:]){5}([0-9A-F]{1,2})$/)
      doPrint = 1;
  
    if (1 == doPrint) {
      gsub (/\./, ":", toPrint);
      gsub (/:/, "_", toPrint);
      print toPrint;
    }
  }
  '|sort|head -1`
  export LOWESTMACADDR
}

fnpath()
{
  linfo "  ${UNAME}: (istat, find <name> -xdev -inum <inode>)"

  NUM_UNIQ_VREG_OPENFILES=`cat ${LSOF_P_FILE}.out | awk '($5 ~ /^VREG$/) {
    printf ("%s %s ", $6, $8);
    $1=$2=$3=$4=$5=$6=$7=$8="";
    printf ("%s\n", $0);
  }' | sort -n | uniq | wc -l`
  linfo "  Number of unique VREG open files (from lsof -lnP -bw -p): ${NUM_UNIQ_VREG_OPENFILES}"

  ISTAT_FIND_XDEV_INUM_FILE=${FNPREFIX}.istat_find_xdev_inum
  cat ${LSOF_P_FILE}.out | awk '($5 ~ /^VREG$/) {
    printf ("%s %s ", $6, $8);
    $1=$2=$3=$4=$5=$6=$7=$8="";
    printf ("%s\n", $0);
  }' | sort -n | uniq | while read DEVICE NODE NAME
  do
    NAME=`echo ${NAME} | sed -e 's/ *(.*)$//g'`
    [ ${LOGSETTING_VERBOSITY} -lt ${LEVEL_DEBUG} ] && printf "."
    ldebug "    From lsof...-p: ${DEVICE} ${NODE} \"${NAME}\""
    echo "### istat (find \"${NAME}\" -xdev -inum ${NODE})" >> ${ISTAT_FIND_XDEV_INUM_FILE}.out
    echo "### istat (find \"${NAME}\" -xdev -inum ${NODE})" >> ${ISTAT_FIND_XDEV_INUM_FILE}.err
    FILEPATH=`find "${NAME}" -xdev -inum ${NODE} 2>&1`
    if [ $? -ne 0 ]
    then
      [ ${LOGSETTING_VERBOSITY} -lt ${LEVEL_DEBUG} ] && printf "\n"
      lerror "    (find \"${NAME}\" -xdev -inum ${NODE}) failed: [${FILEPATH}]"
      echo "(find \"${NAME}\" -xdev -inum ${NODE}) failed: [${FILEPATH}]" >>${ISTAT_FIND_XDEV_INUM_FILE}.err
    else
      ldebug "    Found ${FILEPATH}"
      echo "Found ${FILEPATH}" >> ${ISTAT_FIND_XDEV_INUM_FILE}.out
      istat ${FILEPATH} 1>>${ISTAT_FIND_XDEV_INUM_FILE}.out 2>>${ISTAT_FIND_XDEV_INUM_FILE}.err
    fi
  done
  [ ${LOGSETTING_VERBOSITY} -lt ${LEVEL_DEBUG} ] && printf "\n"
}

