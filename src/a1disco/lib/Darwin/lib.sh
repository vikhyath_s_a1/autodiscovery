fnaddprs()
{
#  PREREQS="${PREREQS} sw_vers system_profiler stat"
  PREREQS="${PREREQS} sw_vers system_profiler"
  export PREREQS
}

fnhost()
{
  linfo "  ${UNAME}: (sw_vers, system_profiler -detailLevel basic)"

  sw_vers 1>${FNPREFIX}.sw_vers.out 2>${FNPREFIX}.sw_vers.err
  system_profiler -detailLevel basic 1>${FNPREFIX}.system_profiler_detailLevel_basic.out 2>${FNPREFIX}.system_profiler_detailLevel_basic.err
}

fnnet()
{
  linfo "  ${UNAME}: (ifconfig -l, ifconfig <ifname>)"

  ifconfig -l 1>${FNPREFIX}.ifconfig_l.out 2>${FNPREFIX}.ifconfig_l.err
  IFCONFIG_L_OUTFILE=${FNPREFIX}.ifconfig_l.out
  
  for ifn in `cat ${IFCONFIG_L_OUTFILE}`
  do
    linfo "  Retrieving configuration of interface ${ifn} ..."
    linfo "    (ifconfig ${ifn}) ..."
    ifconfig ${ifn} 1>${FNPREFIX}.ifconfig_${ifn}.out 2>${FNPREFIX}.ifconfig_${ifn}.err
  done
  
  LOWESTMACADDR=`cat ${FNPREFIX}.ifconfig_*.out | awk ' $1 ~ /^ether$/ {print $2} ' | awk -F: '
  {printf ("%s_%s_%s_%s_%s_%s\n", $1, $2, $3, $4, $5, $6);}' | sort | head -1`
  export LOWESTMACADDR
}

fnpath()
{
  linfo "  ${UNAME}: (stat, find <name> -xdev -inum <inode>)"

  NUM_UNIQ_VREG_OPENFILES=`cat ${LSOF_P_FILE}.out | awk '($5 ~ /^REG$/) {
    printf ("%s %s ", $6, $8);
    $1=$2=$3=$4=$5=$6=$7=$8="";
    printf ("%s\n", $0);
  }' | sort -n | uniq | wc -l`
  linfo "  Number of unique REG open files (from lsof -lnP -bw -p): ${NUM_UNIQ_VREG_OPENFILES}"

  STAT_FIND_XDEV_INUM_FILE=${FNPREFIX}.stat_find_xdev_inum
  cat ${LSOF_P_FILE}.out | awk '($5 ~ /^REG$/) {
    printf ("%s %s ", $6, $8);
    $1=$2=$3=$4=$5=$6=$7=$8="";
    printf ("%s\n", $0);
  }' | sort -n | uniq | while read DEVICE NODE NAME
  do
    [ ${LOGSETTING_VERBOSITY} -lt ${LEVEL_DEBUG} ] && printf "."
    ldebug "    From lsof...-p: ${DEVICE} ${NODE} \"${NAME}\""
    echo "### stat (find \"${NAME}\" -xdev -inum ${NODE})" >> ${STAT_FIND_XDEV_INUM_FILE}.out
    echo "### stat (find \"${NAME}\" -xdev -inum ${NODE})" >> ${STAT_FIND_XDEV_INUM_FILE}.err
    FILEPATH=`find "${NAME}" -xdev -inum ${NODE} 2>&1`
    if [ $? -ne 0 ]
    then
      [ ${LOGSETTING_VERBOSITY} -lt ${LEVEL_DEBUG} ] && printf "\n"
      lerror "    (find \"${NAME}\" -xdev -inum ${NODE}) failed: [${FILEPATH}]"
      echo "(find \"${NAME}\" -xdev -inum ${NODE}) failed: [${FILEPATH}]" >>${STAT_FIND_XDEV_INUM_FILE}.err
    else
      ldebug "    Found ${FILEPATH}"
      echo "Found ${FILEPATH}" >> ${STAT_FIND_XDEV_INUM_FILE}.out
      stat ${FILEPATH} 1>>${STAT_FIND_XDEV_INUM_FILE}.out 2>>${STAT_FIND_XDEV_INUM_FILE}.err
    fi
  done
  [ ${LOGSETTING_VERBOSITY} -lt ${LEVEL_DEBUG} ] && printf "\n"
}

