#!/bin/bash


DISTRONAME=a1disco

VERSION=`git rev-list --count HEAD`
BASEDIR=`git rev-parse --show-toplevel`
DISTROBASE=${BASEDIR}/src/${DISTRONAME}

mkdir -p ${DISTROBASE}/distros
cd ${DISTROBASE}/distros

DISCODISTROVERSION=`printf "${DISTRONAME}-v%02s" ${VERSION}`
DCAPDISTROVERSION=`printf "a1dcap-v%02s" ${VERSION}`

echo "_______________________________________________________________________________"
echo
echo "Creating distribution [${DISCODISTROVERSION}]..."
echo
echo "Removing previous redistributable for this version..."
rm -rvf ${DISCODISTROVERSION}

echo "Creating the root distribution base-dir - ${DISCODISTROVERSION}..."
mkdir -pv ${DISCODISTROVERSION}
mkdir -pv ${DISCODISTROVERSION}/work

echo "Copying the relevant source files over..."
git ls-tree --full-tree -r --name-only HEAD |\
  grep "^src/${DISTRONAME}/" |\
  sed -e 's/^src\/'${DISTRONAME}'\///g' |\
  grep -v "^distros" |\
  grep -v "^work" |\
  while read relPath
  do
    relPath_dirname=`dirname ${relPath}`
    relPath_basename=`basename ${relPath}`
    mkdir -pv ${DISCODISTROVERSION}/${relPath_dirname}
    cp -Rvf ${DISTROBASE}/${relPath} ${DISCODISTROVERSION}/${relPath_dirname}/
  done | sed -e 's/^/  /'

echo "Tarring and gzipping the ${DISCODISTROVERSION} distribution up..."
tar -zcvf ${DISCODISTROVERSION}.tgz ${DISCODISTROVERSION} 2>&1 | sed -e 's/^/  /'

echo "Removing non-a1dcap files from distribution folder..."
rm -rvf ${DISCODISTROVERSION}/*.py
rm -rvf ${DISCODISTROVERSION}/*.txt
rm -rvf ${DISCODISTROVERSION}/inoutsumm.sh
rm -rvf ${DISCODISTROVERSION}/a1dgen.sh
rm -rvf ${DISCODISTROVERSION}/a1discover.sh
rm -rvf ${DISCODISTROVERSION}/components
mv ${DISCODISTROVERSION} ${DCAPDISTROVERSION}

echo "Tarring and gzipping the ${DCAPDISTROVERSION} distribution up..."
tar -zcvf ${DCAPDISTROVERSION}.tgz ${DCAPDISTROVERSION} 2>&1 | sed -e 's/^/  /'

echo "Redistributable archives ready, removing directory..."
rm -rvf ${DCAPDISTROVERSION} | sed -e 's/^/  /'
echo "_______________________________________________________________________________"
echo
echo "Distribution [${DISCODISTROVERSION}] and [${DCAPDISTROVERSION}] have been created @ [${DISTROBASE}/distros/]"
echo "__File_contents_listing_:_${DISCODISTROVERSION}________________________________"
tar -ztvf ${DISCODISTROVERSION}.tgz | sed -e 's/^/  /g'
echo "_______________________________________________________________________________"
echo "__File_contents_listing_:_${DCAPDISTROVERSION}________________________________"
tar -ztvf ${DCAPDISTROVERSION}.tgz | sed -e 's/^/  /g'
echo "_______________________________________________________________________________"
echo

cd -
